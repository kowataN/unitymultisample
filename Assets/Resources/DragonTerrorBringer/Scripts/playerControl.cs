﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System;

public class playerControl : MonoBehaviour
{
	public Animator anim;
	int scream;
	int basicAttack;
	int clawAttack;
	int flameAttack;
	int defend;
	int getHit;
	int sleep;
	int walk;
	int run;
	int takeOff;
	int flyFlameAttack;
	int flyForward;
	int flyGlide;
	int land;
	int stun;
	int idle02;
	// add
	int idle;

	public UnityEvent OnEndCallback = new UnityEvent();


	void Awake()
	{
		anim = GetComponent<Animator>();
		scream = Animator.StringToHash("Scream");
		basicAttack = Animator.StringToHash("Basic Attack");
		clawAttack = Animator.StringToHash("Claw Attack");
		flameAttack = Animator.StringToHash("Flame Attack");
		defend = Animator.StringToHash("Defend");
		getHit = Animator.StringToHash("Get Hit");
		sleep = Animator.StringToHash("Sleep");
		walk = Animator.StringToHash("Walk");
		run = Animator.StringToHash("Run");
		takeOff = Animator.StringToHash("Take Off");
		flyFlameAttack = Animator.StringToHash("Fly Flame Attack");
		flyForward = Animator.StringToHash("Fly Forward");
		flyGlide = Animator.StringToHash("Fly Glide");
		land = Animator.StringToHash("Land");
		stun = Animator.StringToHash("Die");
		idle02 = Animator.StringToHash("Idle02");
		// add
		idle = Animator.StringToHash("Idle01");
	}

	public void BasicAttack()
	{
		anim.CrossFade(basicAttack, 0.0f, 0, 0.0f);
	}

	public void FlameAttack()
	{
		anim.CrossFade(flameAttack, 0.0f, 0, 0.0f);
	}

	public void Defend()
	{
		anim.CrossFade(defend, 0.0f, 0, 0.0f);
	}

	public void GetHit()
	{
		anim.CrossFade(getHit, 0.0f, 0, 0.0f);
	}

	public void Sleep()
	{
		anim.CrossFade(sleep, 0.0f, 0, 0.0f);
	}

	public void Stun()
	{
		anim.CrossFade(stun, 0.0f, 0, 0.0f);
	}

	public void Stunning()
	{
		anim.Play(stun, 0, 1.0f);
	}

	// add
	public void Idle()
	{
		anim.CrossFade(idle, 0.0f, 0, 0.0f);
	}

	public void OnEndAnime()
	{
		OnEndCallback?.Invoke();
	}
}
