using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionMsgCreator : MonoBehaviour
{
    public static List<ActionNodeBase> CreateActionMsg(FloatingInfo info, in SkillModel myselfSkill, in SkillModel targetSkill)
    {
        List<ActionNodeBase> result = new List<ActionNodeBase>();

        // ダメージ値設定
        int myDamage = myselfSkill.Damage;
        int targetDamage = targetSkill.Damage;
        int addPoint = 0;
        if (myselfSkill.Type == SkillTypes.Attack)
        {
            if (targetSkill.Type != SkillTypes.Guard)
            {
                // 攻撃成功
                info.Target.Damage = -myDamage;
                addPoint = Mathf.Min(myDamage / 2, info.Target.CrtPoint);
            }
            else if (targetDamage > 0)
            {
                // 相手のガードにダメージがあるのでダメージを喰らい、ポイントも減る
                info.Myself.Damage = -targetDamage;
                addPoint = -Mathf.Min(myDamage / 2, info.Myself.CrtPoint);
            }
        }
        else if (myselfSkill.Type == SkillTypes.Magic)
        {
            if (targetSkill.Type != SkillTypes.Shield)
            {
                // 攻撃成功
                info.Target.Damage = -myDamage;
                addPoint = Mathf.Min(myDamage * 2, info.Target.CrtPoint);
            }
            else
            {
                addPoint = -Mathf.Min(myDamage * 2, info.Myself.CrtPoint);
            }
        }

        info.Myself.AddPoint = addPoint;
        info.Target.AddPoint = -addPoint;
        info.Myself.CrtPoint += info.Myself.AddPoint;
        info.Target.CrtPoint += info.Target.AddPoint;

        // 頻繁に使うので一旦保持
        string myName = info.Myself.Name;
        string targetName = info.Target.Name;

        result.Add(new MsgActionNode($"{myName}の{myselfSkill.Name}！"));
        result.Add(new AddMPActionNode(info.Myself.Index, -info.UsedMP));
        result.Add(new RotateUnitActionNode(info.Myself.Index, info.Target.Index));
        result.Add(new InvokeSkillActionNode(info.Myself.Index));

        // 打撃攻撃
        if (myselfSkill.Type == SkillTypes.Attack)
        {
            // ガード
            if (targetSkill.Type == SkillTypes.Guard)
            {
                // TODO: カウンター

                result.Add(new GuardActionNode(info.Myself.Index));
                result.Add(new MsgActionNode(
                    (targetSkill.Damage > 0) ? "しかし、相手のカウンター！"
                    : "しかし、相手はガードしている！"));
            }
            else
            {
                // 相手へダメージ
                CreateEnemyDamageList(info).ForEach(x => result.Add(x));

                if (info.Target.IsShield)
                {
                    result.Add(new BreakShieldActionNode(info.Target.Index));
                    result.Add(new MsgActionNode("相手のシールドを破壊した！"));
                }
            }
        }
        else // 魔法攻撃
        {
            // シールド
            if (targetSkill.Type == SkillTypes.Shield)
            {
                // 自身へダメージ
                result.Add(new ReflectionActionNode(info.Myself.Index));
                result.Add(new MsgActionNode("しかし、シールドで反射！"));

                result.Add(new GetHitActionNode(info.Myself.Index));
                result.Add(new AddHPActionNode(info.Myself.Index, -info.Myself.Damage));
                result.Add(new MsgActionNode(
                    $"自身に{Mathf.Abs(info.Myself.Damage).ToString()}ダメージ！"));

                info.Myself.CrtHP += info.Myself.Damage;
                if (info.Myself.CrtHP <= 0)
                {
                    result.Add(new MsgActionNode($"{info.Myself.Name}は気絶した。"));
                }

                result.Add(new AddPointActionNode(info.Myself.Index, -info.Myself.AddPoint));
                result.Add(new AddPointActionNode(info.Target.Index, info.Target.AddPoint));
                result.Add(new UpdateRankingNode());
                var pointMsg = CreatePointMsg(in info);
                if (pointMsg != null)
                {
                    result.Add(pointMsg);
                }
            }
            else
            {
                // 相手へダメージ
                CreateEnemyDamageList(info).ForEach(x => result.Add(x));
            }
        }

        return result;
    }

    private static List<ActionNodeBase> CreateEnemyDamageList(FloatingInfo info)
    {
        List<ActionNodeBase> result = new List<ActionNodeBase>();

        result.Add(new GetHitActionNode(info.Target.Index));
        result.Add(new AddHPActionNode(info.Target.Index, info.Target.Damage));

        info.Target.CrtHP += info.Target.Damage;
        if (info.Target.CrtHP <= 0)
        {
            result.Add(new MsgActionNode($"{info.Target.Name}は気絶した。"));
        }

        result.Add(new AddPointActionNode(info.Target.Index, info.Target.AddPoint));
        result.Add(new AddPointActionNode(info.Myself.Index, info.Myself.AddPoint));
        result.Add(new UpdateRankingNode());
        var pointMsg = CreatePointMsg(in info);
        if (pointMsg != null)
        {
            result.Add(pointMsg);
        }

        return result;
    }

    public static List<ActionNodeBase> CreateActionMsgGuardAndShield(in string name, in ActionInfo myself)
    {
        List<ActionNodeBase> result = new List<ActionNodeBase>();

        string message = $"{name}は";
        ActionNodeType type = ActionNodeType.Shield;
        if (myself.Skill.Type == SkillTypes.Guard)
        {
            message += "ガードをしました。";
            type = ActionNodeType.Guard;
        }
        else
        {
            message += "シールドを展開しました。";
        }

        var info = new FloatingInfo(myself);
        result.Add(new AddMPActionNode(info.Myself.Index, -info.UsedMP));
        if (myself.Skill.Type == SkillTypes.Guard)
        {
            result.Add(new GuardActionNode(info.Myself.Index));
        }
        else
        {
            result.Add(new ShieldActionNode(info.Myself.Index));
        }

        result.Add(new MsgActionNode(message));

        return result;
    }

    public static List<ActionNodeBase> CreateActionMsgCanNotAttackMyStun(in string name) => new List<ActionNodeBase>()
    {
        new MsgActionNode($"{name}は気絶している為、攻撃できませんでした。")
    };

    public static List<ActionNodeBase> CreateActionMsgCanNotAttackTargetStun(in string myName, in string targetName) => new List<ActionNodeBase>()
    {
        new MsgActionNode($"{myName}は{targetName}が気絶している為、攻撃できませんでした。")
    };

    private static ActionNodeBase CreatePointMsg(in FloatingInfo info)
    {
        if (info.Myself.Index == 0) // TODO: 自身のインデックスか判定
        {
            return new MsgActionNode(
                $"あなたは{Mathf.Abs(info.Myself.AddPoint).ToString()}ポイント獲得。");
        }
        else if (info.Target.Index == 0) // TODO: 自身のインデックスか判定
        {
            return new MsgActionNode(
                $"{info.Target.Name}は{Mathf.Abs(info.Target.AddPoint).ToString()}ポイント喪失。");
        }
        return null;
    }
}
