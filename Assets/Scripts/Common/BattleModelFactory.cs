using UniRx;

public static class BattleModelFactory
{
    public static BattleModel CreateBattleModel(BattleView view, BattleData battleData)
    {
        var model = new BattleModel(battleData);

        model.GetCrtTurnObservble()
            .SubscribeToText(view.TurnView.CrtTurn)
            .AddTo(view.gameObject);

        model.GetMaxTurnObservble()
            .SubscribeToText(view.TurnView.MaxTurn)
            .AddTo(view.gameObject);

        return model;
    }
}
