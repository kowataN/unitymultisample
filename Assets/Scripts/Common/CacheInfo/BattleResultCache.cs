using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BattleResultCache
{
    const string CACHE_BATTLE_RESULT = "CACHE_BATTLE_RESULT";
    [SerializeField] private static BattleResultCacheInfo _battleResultData = default;

    public static void Save(BattlePresenter presenter)
    {
        _battleResultData = new BattleResultCacheInfo();
        _battleResultData.List = new List<BattleResultCacheData>();

        for (int i=0; i< GameTypes.MaxBattlePlayerNumber; ++i)
        {
            var player = presenter.GetPlayerModel(i);
            var status = presenter.GetStatusPresenter(i);
            BattleResultCacheData model = new BattleResultCacheData(
                ranking: status.GetRanking(),
                point: player.Point,
                name: player.UnitModel.UnitStatus.Name,
                type: player.PlayerType
            );
            _battleResultData.List.Add(model);
        }
        string json = JsonUtility.ToJson(_battleResultData);
        PlayerPrefs.SetString(CACHE_BATTLE_RESULT, json);
    }

    public static BattleResultCacheInfo Load()
    {
        if (HasData(CACHE_BATTLE_RESULT))
        {
            string json = PlayerPrefs.GetString(CACHE_BATTLE_RESULT);
            _battleResultData = JsonUtility.FromJson<BattleResultCacheInfo>(json);
        }
        else
        {
            CreateNewCache();
        }

        return _battleResultData;
    }

    public static void CreateNewCache()
    {
        PlayerPrefs.DeleteKey(CACHE_BATTLE_RESULT);
        _battleResultData = new BattleResultCacheInfo();
    }

    public static bool HasData(string name) => PlayerPrefs.HasKey(name);

    public static void Delete() => PlayerPrefs.DeleteKey(CACHE_BATTLE_RESULT);
}
