using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UnitSelectCache
{
    const string CACHE_UNIT_SELECT = "CACHE_UNIT_SELECT";
    [SerializeField] private static UnitSelectCacheInfo _unitSelectInfo = default;

    public static void Save(int unitNo, List<int> skillNoList)
    {
        _unitSelectInfo = new UnitSelectCacheInfo() {
            UnitNo = unitNo,
            SkillNoList = skillNoList
        };

        string json = JsonUtility.ToJson(_unitSelectInfo);
        PlayerPrefs.SetString(CACHE_UNIT_SELECT, json);
    }

    public static UnitSelectCacheInfo Load()
    {
        if (HasData(CACHE_UNIT_SELECT))
        {
            string json = PlayerPrefs.GetString(CACHE_UNIT_SELECT);
            _unitSelectInfo = JsonUtility.FromJson<UnitSelectCacheInfo>(json);
        }
        else
        {
            CreateNewCache();
        }

        return _unitSelectInfo;
    }

    public static void CreateNewCache()
    {
        PlayerPrefs.DeleteKey(CACHE_UNIT_SELECT);
        _unitSelectInfo = new UnitSelectCacheInfo();
    }

    public static bool HasData(string name) => PlayerPrefs.HasKey(name);

    public static void Delete(string name) => PlayerPrefs.DeleteKey(name);
}
