﻿using System.Diagnostics;

public static class DebugLogger
{
    [Conditional("UNITY_EDITOR")]
    public static void Log(object obj)
    {
        UnityEngine.Debug.Log(obj);
    }

    [Conditional("UNITY_EDITOR")]
    public static void LogError(object obj)
    {
        UnityEngine.Debug.LogError(obj);
    }
}
