using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class FileIO
{
    /// <summary>
    /// バイナリデータを読み込みます
    /// </summary>
    /// <returns>バイナリデータ</returns>
    /// <param name="filePath">ファイルパス</param>
    public static string Load(string filePath)
    {
        if (File.Exists(filePath) == true)
        {
            using (FileStream fs = File.Open(filePath, FileMode.Open))
            {
                BinaryFormatter bf = new BinaryFormatter();
                string json = (string)bf.Deserialize(fs);
                fs.Close();
                return json;
            }
        }
        return null;
    }

    /// <summary>
    /// JSONデータをバイナリで保存します
    /// </summary>
    /// <returns>保存結果</returns>
    /// <param name="filePath">ファイルパス</param>
    /// <param name="json">保存するJSONデータ</param>
    public static bool SaveJsonData(string filePath, string json)
    {
        if (File.Exists(filePath) == true)
        {
            // ファイルが存在した場合は一旦削除する
            File.Delete(filePath);
        }
        bool ret = false;
        using (FileStream fs = File.Create(filePath))
        {
            // この方法だとバージョン更新などでの構造変更に対応できない
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(fs, json);
            fs.Close();
            ret = true;
        }
        return ret;
    }
}
