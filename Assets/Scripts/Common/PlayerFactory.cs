using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static class PlayerFactory
{
    static public BattlePlayerModel CreatePlayer(int index, string playerName, int unitNo)
    {
        BattlePlayerModel res = new BattlePlayerModel();

        UnitModel unit = UnitStorage.Inst.GetDataFromNo(unitNo);
        if (unit == null)
        {
            DebugLogger.LogError($"IsNull UnitModel No[{unitNo}]");
        }

        res.PlayerModel = PlayerModel.Create(index, playerName, unitNo);
        res.PlayerType = PlayerType.Player;
        res.HP = unit.UnitStatus.Status.HP;
        res.MP = unit.UnitStatus.Status.MP;

        return res;
    }

    static public BattlePlayerModel CreateCpu(int index, int unitNo)
    {
        BattlePlayerModel res = new BattlePlayerModel();

        EnemyModel enemy = EnemyStorage.Inst.GetDataFromNo(unitNo);
        if (enemy == null)
        {
            DebugLogger.LogError($"IsNull UnitModel No[{unitNo}]");
        }

        res.PlayerModel = PlayerModel.Create(index,
            enemy.Unit.UnitStatus.Name,
            enemy.Unit.UnitStatus.No);
        res.PlayerType = PlayerType.CPU;
        res.HP = enemy.Unit.UnitStatus.Status.HP;
        res.MP = enemy.Unit.UnitStatus.Status.MP;

        res.UnitModel.SetSkillSet(enemy.Unit.UnitStatus.SkillSetNo);

        return res;
    }
}
