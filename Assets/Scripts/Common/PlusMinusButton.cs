﻿using UnityEngine;
using UnityEngine.UI;

public class PlusMinusButton : MonoBehaviour
{
    public Text Value;
    public Button Plus;
    public Button Minus;

    // Use this for initialization
    void Start()
    {
        Value.text = "0";
    }
}
