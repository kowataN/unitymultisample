﻿using UnityEngine;

public class PrefabCreator
{
    private static readonly string[] _PrefabUnitName = {
        "RedDragon", "GreenDragon", "GreyDragon" ,"AlbinoDragon", "RedDragon", "GreenDragon", "AlbinoDragon" };

    private static readonly string _PrefabSkillIconName = "Prefabs/PartsSkillIcon";


    public static GameObject CreateUnit(Attributes atr, Transform parent)
    {
        string prefabPath = "DragonTerrorBringer/Prefabs/" + _PrefabUnitName[(int)atr];
        GameObject prefab = Resources.Load(prefabPath) as GameObject;
        if (prefab == null)
        {
            DebugLogger.LogError("存在しないPrefabファイル:" + prefabPath);
            return null;
        }

        return GameObject.Instantiate(prefab, parent);
    }

    public static GameObject CreateSkillIcon(Transform parent)
    {
        GameObject prefab = Resources.Load(_PrefabSkillIconName) as GameObject;
        if (prefab == null)
        {
            DebugLogger.LogError("存在しないPrefabファイル :" + _PrefabSkillIconName);
            return null;
        }

        return GameObject.Instantiate(prefab, parent);
    }
}
