using System;
using System.Collections.Generic;
using System.Linq;

public class EnemyStorage : SingletonBase<EnemyStorage>
{
    private List<EnemyModel> _enemyList = new List<EnemyModel>();

    public EnemyStorage()
    {
        //DebugLogger.Log("EnemyStorage::EnemyStorage");
        Init();
    }

    /// <summary>
    /// ランダムでエネミーモデルを返します
    /// </summary>
    /// <returns>The data from random.</returns>
    public EnemyModel GetDataFromRandom() => _enemyList.OrderBy(x => Guid.NewGuid()).FirstOrDefault();

    public EnemyModel GetDataFromNo(int value) => new EnemyModel(_enemyList.Where(x => x.EnemyNo == value).FirstOrDefault());

    private void Init()
    {
        CsvLoader loader = new CsvLoader();
        bool res = loader.Load(Utility.Utility.GetCsvDataDirectoryName() + "Enemy", CsvLoader.Delimiter.Tab);
        if (res == false)
        {
            return;
        }

        int headerCount = loader.Headers.Count;
        Dictionary<int, List<string>> csvdata = loader.GetDatas();
        int useIndex = loader.GetHeaderIndexFromString("USE");
        foreach (KeyValuePair<int, List<string>> pair in csvdata)
        {
            if (headerCount != pair.Value.Count || int.Parse(pair.Value[useIndex]) == 0)
            {
                // 件数が違うもしくは使用できないデータは無視
                continue;
            }
            //DebugLogger.Log("Create Enemy No:" + pair.Value[loader.GetHeaderIndexFromString("NO")] +
            //"  UnitNo:" + pair.Value[loader.GetHeaderIndexFromString("UNIT_NO")]);

            EnemyModel model = new EnemyModel {
                EnemyNo = int.Parse(pair.Value[loader.GetHeaderIndexFromString("NO")]),
                Unit = UnitStorage.Inst.GetDataFromNo(
                    int.Parse(pair.Value[loader.GetHeaderIndexFromString("UNIT_NO")]))
            };

            if (model.Unit == null)
            {
                DebugLogger.LogError("存在しないユニットが指定されています UnitNo:" + pair.Value[loader.GetHeaderIndexFromString("UNIT_NO")]);
            }

            var no = model.Unit.UnitStatus.No;
            var atr = model.Unit.UnitStatus.Atr;
            var tribe = model.Unit.UnitStatus.Tribe;
            var skillSet = int.Parse(pair.Value[loader.GetHeaderIndexFromString("SKILL_SET")]);
            var name = pair.Value[loader.GetHeaderIndexFromString("NAME")];

            //DebugLogger.Log($"EnemyNo[{model.EnemyNo}] unitNo[{no}] name[{name}]");

            // エネミー情報で上書き
            model.Unit.SetParameter(
                no,
                atr,
                tribe,
                new BasicStatus {
                    HP = int.Parse(pair.Value[loader.GetHeaderIndexFromString("HP")]),
                    MP = int.Parse(pair.Value[loader.GetHeaderIndexFromString("MP")]),
                    Attack = int.Parse(pair.Value[loader.GetHeaderIndexFromString("ATK")]),
                    Magic = int.Parse(pair.Value[loader.GetHeaderIndexFromString("MGC")]),
                    Defense = int.Parse(pair.Value[loader.GetHeaderIndexFromString("DEF")]),
                    Luck = int.Parse(pair.Value[loader.GetHeaderIndexFromString("LUC")])
                },
                skillSet,
                name
            );

            _enemyList.Add(model);
        }
    }
}
