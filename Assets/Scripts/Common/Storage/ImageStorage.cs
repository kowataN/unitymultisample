﻿using UnityEngine;

public class ImageStorage : SingletonMonoBhv<ImageStorage>
{
    [SerializeField] private Sprite[] _skillIsonImages = null;
    [SerializeField] private Sprite[] _shieldImages = null;

    public Sprite GetSkillIcon(int index) => _skillIsonImages[index];

    public Sprite GetShieldImage(int index) => _shieldImages[index];
}
