using System.Collections.Generic;
using UnityEngine;

public class SkillSetStorage : SingletonBase<SkillSetStorage>
{
    private Dictionary<int, List<SkillModel>> _skillSetList = new Dictionary<int, List<SkillModel>>();

    public SkillSetStorage()
    {
        Init();
    }

    public Dictionary<int, List<SkillModel>> GetList() => _skillSetList;

    public List<SkillModel> GetSkillSet(int no) => _skillSetList[no];

    private void Init()
    {
        CsvLoader loader = new CsvLoader();
        bool res = loader.Load(Utility.Utility.GetCsvDataDirectoryName() + "SkillSet", CsvLoader.Delimiter.Tab);
        if (res == false)
        {
            Debug.LogError("[SkillSetStorage::Init] SkillSet.csvの読み込み失敗");
            return;
        }

        int headerCount = loader.Headers.Count;
        Dictionary<int, List<string>> csvdata = loader.GetDatas();
        foreach (KeyValuePair<int, List<string>> pair in csvdata)
        {
            if (headerCount != pair.Value.Count)
            {
                continue;
            }

            int setNo = int.Parse(pair.Value[loader.GetHeaderIndexFromString("SET_NO")]);
            string skills = pair.Value[loader.GetHeaderIndexFromString("SKILLS")];

            List<SkillModel> models = new List<SkillModel>();
            string[] datas = skills.Split('|');
            foreach (var data in datas)
            {
                int skillno = int.Parse(data);
                SkillModel skill = SkillStorage.Inst.GetData(skillno);
                if (skill.No != 0)
                {
                    models.Add(skill);
                }
                else
                {
                    Debug.LogError("[SkillSetStorage::Init] Not Found:" + skillno.ToString());

                }
            }

            _skillSetList.Add(setNo, models);
        }
    }
}
