﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SkillStorage : SingletonBase<SkillStorage>
{
    private static List<SkillModel> _skillList = new List<SkillModel>();

    public SkillStorage()
    {
        Init();
    }

    /// <summary>
    /// スキルデータを返します
    /// </summary>
    /// <returns>スキルデータ</returns>
    public List<SkillModel> SkillList => _skillList;

    public SkillModel GetData(int no) => SkillList.Where(x => x.No == no).FirstOrDefault();

    private void Init()
    {
        CsvLoader loader = new CsvLoader();
        bool res = loader.Load(Utility.Utility.GetCsvDataDirectoryName() + "Skill", CsvLoader.Delimiter.Tab);
        if (res == false)
        {
            Debug.LogError("[SkillStorage::Init] Skill.csvの読み込み失敗");
            return;
        }

        int headerCount = loader.Headers.Count;
        Dictionary<int, List<string>> csvdata = loader.GetDatas();
        int useIndex = loader.GetHeaderIndexFromString("USE");
        foreach (KeyValuePair<int, List<string>> pair in csvdata)
        {
            if (headerCount != pair.Value.Count || int.Parse(pair.Value[useIndex]) == 0)
            {
                // 件数が違うもしくは使用できないデータは無視
                continue;
            }
            var optionalString = pair.Value[loader.GetHeaderIndexFromString("OPTIONAL")];
            var optionalList = JsonUtility.FromJson<OptionalList>(optionalString);
            SkillModel model = new SkillModel() {
                No = int.Parse(pair.Value[loader.GetHeaderIndexFromString("NO")]),
                Atr = (Attributes)int.Parse(pair.Value[loader.GetHeaderIndexFromString("ATR")]),
                Type = (SkillTypes)int.Parse(pair.Value[loader.GetHeaderIndexFromString("TYPE")]),
                Rank = int.Parse(pair.Value[loader.GetHeaderIndexFromString("RANK")]),
                Speed = int.Parse(pair.Value[loader.GetHeaderIndexFromString("SPEED")]),
                Cost = int.Parse(pair.Value[loader.GetHeaderIndexFromString("COST")]),
                Name = pair.Value[loader.GetHeaderIndexFromString("NAME")],
                Comment = pair.Value[loader.GetHeaderIndexFromString("COMMENT")],
                Target = (SkillTargetTypes)int.Parse(pair.Value[loader.GetHeaderIndexFromString("TARGET")]),
                Member = int.Parse(pair.Value[loader.GetHeaderIndexFromString("MEMBER")]),
                Turn = int.Parse(pair.Value[loader.GetHeaderIndexFromString("TURN")]),
                Damage = int.Parse(pair.Value[loader.GetHeaderIndexFromString("DAMAGE")]),
                Motion = pair.Value[loader.GetHeaderIndexFromString("MOTION")],
                OptionalList = optionalList
            };
            _skillList.Add(model);
        }
    }
}
