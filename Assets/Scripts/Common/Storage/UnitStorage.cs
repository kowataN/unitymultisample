using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UnitStorage : SingletonBase<UnitStorage>
{
    private List<UnitModel> _unitList = new List<UnitModel>();

    public UnitStorage()
    {
        Init();
    }

    public List<UnitModel> UnitList => _unitList;

    public UnitModel GetDataFromIndex(int index) => new UnitModel(_unitList.ElementAt(index));

    public UnitModel GetDataFromNo(int no)
    {
        IEnumerable<UnitModel> ret = _unitList.Where(x => x.UnitStatus.No == no);
        return new UnitModel((ret.Count() > 0) ? ret.ElementAt(0) : null);
    }

    private void Init()
    {
        CsvLoader loader = new CsvLoader();
        bool res = loader.Load(Utility.Utility.GetCsvDataDirectoryName() + "Unit", CsvLoader.Delimiter.Tab);
        if (res == false)
        {
            Debug.LogError("[UnitStorage::Init] Unit.csvの読み込み失敗");
            return;
        }

        int headerCount = loader.Headers.Count;
        Dictionary<int, List<string>> csvdata = loader.GetDatas();
        int useIndex = loader.GetHeaderIndexFromString("USE");
        foreach (KeyValuePair<int, List<string>> pair in csvdata)
        {
            if (headerCount != pair.Value.Count || int.Parse(pair.Value[useIndex]) == 0)
            {
                // 件数が違うもしくは使用できないデータは無視
                continue;
            }
            UnitModel model = new UnitModel();
            model.SetParameter(
                int.Parse(pair.Value[loader.GetHeaderIndexFromString("NO")]),
                (Attributes)int.Parse(pair.Value[loader.GetHeaderIndexFromString("ATR")]),
                (Tribes)int.Parse(pair.Value[loader.GetHeaderIndexFromString("TRIBE")]),
                new BasicStatus {
                    HP = int.Parse(pair.Value[loader.GetHeaderIndexFromString("HP")]),
                    MP = int.Parse(pair.Value[loader.GetHeaderIndexFromString("MP")]),
                    Attack = int.Parse(pair.Value[loader.GetHeaderIndexFromString("ATK")]),
                    Magic = int.Parse(pair.Value[loader.GetHeaderIndexFromString("MGC")]),
                    Defense = int.Parse(pair.Value[loader.GetHeaderIndexFromString("DEF")]),
                    Luck = int.Parse(pair.Value[loader.GetHeaderIndexFromString("LUC")])
                },
                int.Parse(pair.Value[loader.GetHeaderIndexFromString("SKILL_SET")]),
                pair.Value[loader.GetHeaderIndexFromString("NAME")]
            );
            /*            model.SetAddParameter(
                            new BasicStatus {
                                Hp = int.Parse(pair.Value[loader.GetHeaderIndexFromString("ADD_HP")]),
                                Mp = int.Parse(pair.Value[loader.GetHeaderIndexFromString("ADD_MP")]),
                                Atk = int.Parse(pair.Value[loader.GetHeaderIndexFromString("ADD_ATK")]),
                                Mgc = int.Parse(pair.Value[loader.GetHeaderIndexFromString("ADD_MGC")]),
                                Def = int.Parse(pair.Value[loader.GetHeaderIndexFromString("ADD_DEF")]),
                                Luc = int.Parse(pair.Value[loader.GetHeaderIndexFromString("ADD_LUC")])
                            }
                        );
            */
            int skillSetNo = int.Parse(pair.Value[loader.GetHeaderIndexFromString("SKILL_SET")]);
            var skillset = SkillSetStorage.Inst.GetSkillSet(skillSetNo);
            foreach (var skill in skillset)
            {
                model.SkillList.Add(skill);
            }
            _unitList.Add(model);
        }
    }
}
