using UnityEngine;

namespace Utility
{
    public static class Utility
    {
        /// <summary>
        /// リソースのディレクトリパスを返します
        /// </summary>
        /// <returns>リソースのディレクトリパス</returns>
        public static string GetResourcesPath()
        {
            return Application.dataPath + "/Resources/";
        }

        public static string GetPrefabsPath()
        {
            return Application.dataPath + "/Prefabs/";
        }

        /// <summary>
        /// Csvデータのディレクトリ名を返します
        /// </summary>
        /// <returns>CSVデータのディレクトリ名</returns>
        public static string GetCsvDataDirectoryName()
        {
            return "CsvDatas/";
        }

        /// <summary>
        /// セーブデータのディレクトリ名を返します
        /// </summary>
        /// <returns>セーブデータのディレクトリ名</returns>
        public static string GetSaveDataDirectoryName()
        {
            return "SaveData/";
        }

        /// <summary>
        /// CSVデータのディレクトリパスを返します
        /// </summary>
        /// <returns>csvデータのディレクトリパス</returns>
        public static string GetCsvDataPath()
        {
            return GetResourcesPath() + GetCsvDataDirectoryName();
        }

        /// <summary>
        /// セーブデータのディレクトリパスを返します
        /// </summary>
        /// <returns>セーブデータのディレクトリパス</returns>
        public static string GetSaveDataPath()
        {
            return GetResourcesPath() + GetSaveDataDirectoryName();
        }

        public static string GetSaveDataFullPath()
        {
            return GetSaveDataPath() + "savedata.bin";
        }
    }
}
