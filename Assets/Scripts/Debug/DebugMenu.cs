﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

using UnityEngine;
using UnityEngine.UI;

public class DebugMenu : MonoBehaviour
{
    [SerializeField] private Dropdown _listCamera;

    private GameObject[] _cameras = new GameObject[4];

    // Use this for initialization
    private void Awake()
    {
        // シーン遷移してもデバッグメニューが消えないようにする
        DontDestroyOnLoad(gameObject);

        Scene scene = SceneManager.GetSceneByName("Battle");
        GameObject[] game_objects = scene.GetRootGameObjects();

        // ここからは泥臭い方法でオブジェクトを取得する デバッグメニューなのでOK
        foreach (GameObject obj in game_objects)
        {
            switch (obj.tag)
            {
                case "Camera1":
                    _cameras[0] = obj;
                    break;
                case "Camera2":
                    _cameras[1] = obj;
                    break;
                case "Camera3":
                    _cameras[2] = obj;
                    break;
                case "Camera4":
                    _cameras[3] = obj;
                    break;
                default:
                    break;
            }
        }
    }

    public void OnChengedCameraList(int val)
    {
        DebugLogger.Log("DM OnChengedCameraList val:" + val);
        for (int i = 0; i < _cameras.Length; ++i)
        {
            // 自分用のカメラのみ有効にする
            _cameras[i].SetActive(val == i);
        }
    }
}
