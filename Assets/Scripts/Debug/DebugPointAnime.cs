using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class DebugPointAnime : SingletonMonoBhv<DebugPointAnime>
{
    public BattlePresenter Presenter
    {
        get; set;
    }

    public void OnClickButton()
    {
        var statusPre = Presenter.GetStatusPresenter(0);

        StartCoroutine(statusPre.AddPointAnime(100, 1));
    }
}
