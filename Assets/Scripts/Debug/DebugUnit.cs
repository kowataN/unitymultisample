using System.Collections;
using System.Collections.Generic;
using UniRx.Triggers;
using UnityEngine;

public class DebugUnit : MonoBehaviour
{
    [SerializeField] private GameObject _unit = default;
    [SerializeField] private Animator _animator = default;
    private AnimatorStateInfo _stateInfo = default;

    private enum Motion
    {
        Idle,Atk,Def,Stun,Stunning,GitHit
    }
    private Motion _motion = Motion.Idle;

    private void Start()
    {
        _stateInfo = _animator.GetCurrentAnimatorStateInfo(0);
        var trigger = _animator.GetBehaviour<ObservableStateMachineTrigger>();
        playerControl pc = _unit.GetComponent<playerControl>();
        pc.OnEndCallback.AddListener(OnEndCallback);
    }

    public void OnIdle()
    {
        _motion = Motion.Idle;
        _animator.Play("Idle01", 0, 0.0f);
    }

    public void OnAtk()
    {
        _motion = Motion.Atk;
        _animator.CrossFade("Basic Attack", 0.0f, 0, 0.0f);
    }

    public void OnDef()
    {
        _motion = Motion.Def;
        _animator.CrossFade("Defend", 0.0f, 0, 0.0f);
    }

    public void OnStun()
    {
        _motion = Motion.Stun;
        _animator.CrossFade("Die", 0.0f, 0, 0.0f);
    }

    public void OnStunning()
    {
        _motion = Motion.Stunning;
        _animator.Play("Die", 0, 1.0f);
    }

    public void OnDmg()
    {
        _motion = Motion.GitHit;
        _animator.CrossFade("Get Hit", 0.0f, 0, 0.0f);
    }

    public void OnEndCallback()
    {
        string mot;
        switch (_motion)
        {
            case Motion.Atk:
                mot = "攻撃";
                break;
            case Motion.Def:
                mot = "防御";
                break;
            case Motion.GitHit:
                mot = "ダメージ";
                break;
            case Motion.Stun:
                mot = "気絶";
                break;
            case Motion.Stunning:
                mot = "気絶中";
                break;
            default:
                mot = "";
                break;
        }
        Debug.Log($"DebugUnit.OnEndCallback[{mot}]");
    }
}
