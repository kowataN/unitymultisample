﻿/// <summary>
/// 行動情報
/// </summary>
public struct ActionInfo
{
    /// <summary>
    /// 自身のインデックス
    /// </summary>
    public int MyIndex;
    /// <summary>
    /// 対象のインデックス
    /// </summary>
    public int TargetIndex;
    /// <summary>
    /// 使用するスキル情報
    /// </summary>
    public SkillModel Skill;

    public ActionInfo(int myIndex, int targetIndex, SkillModel skillModel)
    {
        MyIndex = myIndex;
        TargetIndex = targetIndex;
        Skill = skillModel;
    }
}
