﻿public enum Attributes {
    /// <summary>
    /// 無属性(0)
    /// </summary>
    None = 0,

    /// <summary>
    /// 火属性(1)
    /// </summary>
    Fire,

    /// <summary>
    /// 水属性(2)
    /// </summary>
    Water,

    /// <summary>
    /// 雷属性(3)
    /// </summary>
    Thunder,

    /// <summary>
    /// 金属性(4)
    /// </summary>
    Metal,

    /// <summary>
    /// 光属性(5)
    /// </summary>
    Light,

    /// <summary>
    /// 闇属性(6)
    /// </summary>
    Darkness
};

/// <summary>
/// 属性
/// </summary>
public static class Attribute {

    #region TODO そのうち外部定義にする
    private static readonly string[] _strings = {
        "無", "火", "水", "雷", "金", "光", "闇"
    };

    /// <summary>
    /// 指定属性の文字列を返します
    /// </summary>
    /// <returns>属性文字</returns>
    /// <param name="atr">Atr.</param>
    public static string GetString(Attributes atr) => _strings[(int)atr];

    /// <summary>
    /// 属性文を配列で返します
    /// </summary>
    /// <returns>属性文字の配列</returns>
    public static string[] GetStrings() => _strings;
    #endregion
}
