using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Entity/BattleData")]
public sealed class BattleData : ScriptableObject
{
    [Header("初期HP")]
    public int InitialHP = 300;

    [Header("初期MP")]
    public int InitialMP = 100;

    [Header("開始ポイント")]
    public int InitStartPoint = 300;

    [Header("最大ターン数")]
    public int MaxBattleTurn = 6;

    [Header("コマンド入力待受時間")]
    public float InputLatency = 5.0f;

    [Header("MP回復値")]
    public int RecoveryMPValue = 20;
}
