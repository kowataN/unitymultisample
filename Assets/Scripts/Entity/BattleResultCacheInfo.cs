using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum CacheDataState
{
    NewData,
    Loaded,
    Displayed
}

[System.Serializable]
public class BattleResultCacheData
{
    public int Ranking = 1;
    public string Name = "";
    public int Point = 100;
    public PlayerType Type = PlayerType.Player;

    public BattleResultCacheData(int ranking, string name, int point, PlayerType type)
    {
        Ranking = ranking;
        Name = name;
        Point = point;
        Type = type;
    }
}

[System.Serializable]
public class BattleResultCacheInfo 
{
    public CacheDataState State = CacheDataState.NewData;
    public List<BattleResultCacheData> List = new List<BattleResultCacheData>();
}
