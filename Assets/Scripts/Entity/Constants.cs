using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// プレイヤーマルチ種別
/// </summary>
public enum PlayerMultiType
{
    /// <summary>
    /// ホスト
    /// </summary>
    Host,
    /// <summary>
    /// ゲスト
    /// </summary>
    Guest
}

/// <summary>
/// プレイヤー種別
/// </summary>
public enum PlayerType
{
    /// <summary>
    /// プレイヤー
    /// </summary>
    Player,
    /// <summary>
    /// CPU
    /// </summary>
    CPU
}

/// <summary>
/// バトル結果
/// </summary>
public enum BattleResult
{
    /// <summary>
    /// 引き分け
    /// </summary>
    Draw,
    /// <summary>
    /// 勝利
    /// </summary>
    Win,
    /// <summary>
    /// 敗北
    /// </summary>
    Lose,
}

/// <summary>
/// 画面の種類
/// </summary>
public enum ViewType
{
    /// <summary>
    /// タイトル
    /// </summary>
    Title,
    /// <summary>
    /// 設定
    /// </summary>
    Setting,
    /// <summary>
    /// マッチング
    /// </summary>
    Matching,
    /// <summary>
    /// バトル
    /// </summary>
    Battle,
    /// <summary>
    /// ランク結果
    /// </summary>
    RankResult,
}

/// <summary>
/// バトルモード
/// </summary>
public enum BattleMode
{
    /// <summary>
    /// 一人
    /// </summary>
    Solo,
    /// <summary>
    /// マルチ
    /// </summary>
    Multi,
}

/// <summary>
/// マルチモード
/// </summary>
public enum MultiMode
{
    /// <summary>
    /// 通常
    /// </summary>
    Normal,
    /// <summary>
    /// ランク対戦
    /// </summary>
    Rank
}

/// <summary>
/// ユニットモーション種別
/// </summary>
public enum UnitMotionType
{
    Idle,
    Defense,
    Attack,
    Magic,
    GetHit,
    Stun,
    Stunning,
};

public enum AbnormalType
{
    Normal,
    Stun,
    Max
}

/// <summary>
/// レート変動テーブル
/// </summary>
public class FloatingRate
{
    /// <summary>
    /// 勝利時の変動値
    /// </summary>
    public int WinValue { get; set; } = 0;

    /// <summary>
    /// 敗北時の変動値
    /// </summary>
    public int LoseValue { get; set; } = 0;
}

/// <summary>
/// ユニットの向き情報
/// </summary>
public struct RotateInfo
{
    public Quaternion StartRot;
    public Quaternion TargetRot;
    public float CountTime;
}

public static class ParamValueTypes
{
    /// <summary>
    /// 最大HP
    /// </summary>
    public static readonly int MaxValueHP = 999;
    /// <summary>
    /// 最大MP
    /// </summary>
    public static readonly int MaxValueMP = 300;

    public static readonly Color StartColor = new Color(1, 1, 1, 0);
}

public static class GameTypes
{
    /// <summary>
    /// 最大プレイヤー数
    /// </summary>
    public static readonly int MaxBattlePlayerNumber = 4;

    public static readonly Dictionary<int, Dictionary<int, float>> RotateTbl = new Dictionary<int, Dictionary<int, float>>() {
            { 0,  new Dictionary<int, float>() {
                { 1, 0f },
                { 2, 45f },
                { 3, 90f }
            } },
            { 1, new Dictionary<int, float>() {
                { 0, 180f },
                { 2, 90f },
                { 3, 135f }
            } },
            { 2, new Dictionary<int, float>() {
                { 0, 225f },
                { 1, 270f },
                { 3, 180f }
            } },
            { 3, new Dictionary<int, float>() {
                { 0, 270f },
                { 1, 335f },
                { 2, 359f }
            } },
        };
}
