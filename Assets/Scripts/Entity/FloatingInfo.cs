/// <summary>
/// 変動情報
/// </summary>
public struct FloatingInfo
{
    public struct Base
    {
        /// <summary>インデックス</summary>
        public int Index;
        /// <summary>名前</summary>
        public string Name;
        /// <summary>現在HP</summary>
        public int CrtHP;
        /// <summary>ダメージ</summary>
        public int Damage;
        /// <summary>現在ポイント</summary>
        public int CrtPoint;
        /// <summary>加算ポイント</summary>
        public int AddPoint;
        /// <summary>シールドを展開しているか</summary>
        public bool IsShield;

        public Base(int idx)
        {
            Index = idx;
            Name = default;
            CrtHP = 0;
            Damage = 0;
            CrtPoint = 0;
            AddPoint = 0;
            IsShield = false;
        }
    };

    public Base Myself;
    public Base Target;

    /// <summary>消費MP</summary>
    public int UsedMP;

    public FloatingInfo(in ActionInfo info)
    {
        Myself = new Base(info.MyIndex);
        Target = new Base(info.TargetIndex);
        this.UsedMP = info.Skill.Cost;
    }
}
