﻿using System;

/// <summary>
/// ユニット情報
/// </summary>
[Serializable]
public struct UnitInfo
{
    public UnitModel UnitModel;
    public BasicStatus AddStatus;
};

/// <summary>
/// プレイヤー情報
/// </summary>
[Serializable]
public struct PlayerInfo
{
    public string Name;
    public UnitModel UnitModel;
};
