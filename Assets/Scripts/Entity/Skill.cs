﻿/// <summary>
/// スキルタイプ
/// </summary>
public enum SkillTypes
{
    /// <summary>なし</summary>
    None = 0,

    /// <summary>攻撃</summary>
    Attack,

    /// <summary>魔法</summary>
    Magic,

    /// <summary>ガード</summary>
    Guard,

    /// <summary>シールド</summary>
    Shield,

    /// <summary>補助</summary>
    Assist,
};

/// <summary>
/// 対象タイプ
/// </summary>
[System.SerializableAttribute]
public enum SkillTargetTypes
{
    /// <summary>自分自身</summary>
    MySelf = 0,

    /// <summary>敵</summary>
    Enemy = 1,
}

/// <summary>
/// オプション情報
/// </summary>
[System.Serializable]
public struct OptionalInfo
{
    /// <summary>対象</summary>
    public SkillTargetTypes Target;

    /// <summary>対象人数</summary>
    public int Member;

    /// <summary>パラメータ</summary>
    public string Param;

    /// <summary>効果値</summary>
    public int Value;

    /// <summary>インデックス</summary>
    public int Index;
}

public static class TypeString
{
    private static readonly string[] _String = {
        "なし", "攻撃", "魔法", "ガード", "シールド", "補助"
    };

    public static string GetString(SkillTypes type) => _String[(int)type];

    public static string[] GetStrings() => _String;
}

public static class Rank
{
    private static string[] _SkillRankString = {
        "☆1", "☆2", "☆3", "☆4", "☆5"
    };

    public static string GetSkillRankString(int index) => _SkillRankString[index];

    public static string[] GetSkillRankStrings() => _SkillRankString;
}

public static class Speed
{
    private static string[] _SkillSpeedString = {
        "超遅い", "遅い", "普通", "速い", "最速"
    };

    public static string GetSkillSpeedString(int index) => _SkillSpeedString[index];

    public static string[] GetSkillSpeedStrings() => _SkillSpeedString;
}
