﻿public enum Params
{
    HP, MP, Attack, Magic, Defense, Luck,
    Max
};

/// <summary>
/// 種族の定義
/// </summary>
public enum Tribes
{
    // なし(0)、基本(1), 淡(2), 濃(3), 強化種(4), 変異種(5)
    None = 0, Basic, Light, Dark, Enhances, Variant
};

/// <summary>
/// 基礎ステータス
/// </summary>
public struct BasicStatus
{
    /// <summary>
    /// HP
    /// </summary>
    public int HP;

    /// <summary>
    /// MP
    /// </summary>
    public int MP;

    /// <summary>
    /// 物理攻撃力
    /// </summary>
    public int Attack;

    /// <summary>
    /// 魔法攻撃力
    /// </summary>
    public int Magic;

    /// <summary>
    /// 防御力
    /// </summary>
    public int Defense;

    /// <summary>
    /// 運
    /// </summary>
    public int Luck;
}

/// <summary>
/// ユニット情報
/// </summary>
public struct UnitStatus
{
    /// <summary>
    /// ユニット番号
    /// </summary>
    public int No;

    /// <summary>
    /// 属性
    /// </summary>
    public Attributes Atr;

    /// <summary>
    /// 種族
    /// </summary>
    public Tribes Tribe;

    /// <summary>
    /// ステータス
    /// </summary>
    public BasicStatus Status;

    /// <summary>
    /// スキルセット番号
    /// </summary>
    public int SkillSetNo;

    /// <summary>
    /// 使用フラグ
    /// </summary>
    public bool Use;

    /// <summary>
    /// 名前
    /// </summary>
    public string Name;

    public UnitStatus(UnitStatus org)
    {
        this.No = org.No;
        this.Atr = org.Atr;
        this.Tribe = org.Tribe;
        this.Status = org.Status;
        this.SkillSetNo = org.SkillSetNo;
        this.Use = org.Use;
        this.Name = org.Name;
    }
}

/// <summary>
/// 種族
/// </summary>
public static class Tribe
{

    #region TODO そのうち外部定義にする
    private static readonly string[] _strings = {
        "なし", "基本種", "淡血種", "濃血種", "強化種", "変異種"
    };

    public static string GetString(Tribes atr) => _strings[(int)atr];

    public static string[] GetStrings() => _strings;
    #endregion
}
