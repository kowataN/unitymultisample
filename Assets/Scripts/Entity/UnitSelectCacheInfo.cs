using System.Collections.Generic;

[System.Serializable]
public class UnitSelectCacheInfo
{
    public int UnitNo = 0;
    public List<int> SkillNoList;
}
