using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

public class BattleModel
{
    private IntReactiveProperty _crtTurn = new IntReactiveProperty(1);
    public int CrtTurn => _crtTurn.Value;
    public System.IObservable<int> GetCrtTurnObservble() => _crtTurn.AsObservable();

    private IntReactiveProperty _maxTurn = new IntReactiveProperty();
    public int MaxTurn => _maxTurn.Value;
    public System.IObservable<int> GetMaxTurnObservble() => _maxTurn.AsObservable();

    public List<BattlePlayerModel> BattlePlayers
    {
        get; private set;
    } = new List<BattlePlayerModel>();

    private int _recoveryMPValue = default;
    public int RecoveryMPValue => _recoveryMPValue;

    private int _startPoint = default;
    public int StartPoint => _startPoint;

    private float _inputLatency = default;
    public float InputLatency => _inputLatency;

    public BattleModel(BattleData battleData)
    {
        BattlePlayers.ForEach(x => x = new BattlePlayerModel());

        _recoveryMPValue = battleData.RecoveryMPValue;
        _inputLatency = battleData.InputLatency;
        _startPoint = battleData.InitStartPoint;
        _maxTurn.Value = battleData.MaxBattleTurn;
    }

    public void CreatePlayer(int index, string playerName, int unitNo)
    {
        var player = PlayerFactory.CreatePlayer(index, playerName, unitNo);
        player.Point = StartPoint;
        BattlePlayers.Add(player);
    }

    public void CreateCpu()
    {
        // TODO: とりあえず仮定義
        int[] unitNo = new int[3] { 2, 7, 16 };

        int cpuIndx = 1;
        int createCount = GameTypes.MaxBattlePlayerNumber - 1;

        for (int i = 0; i < createCount; ++i, ++cpuIndx)
        {
            var cpu = PlayerFactory.CreateCpu(cpuIndx, unitNo[i]);
            cpu.Point = StartPoint;
            BattlePlayers.Add(cpu);
        }
    }

    public int CpuNumber => BattlePlayers.Count(x => x.PlayerType == PlayerType.CPU);

    public void AddTurn() => _crtTurn.Value = Mathf.Min(CrtTurn + 1, _maxTurn.Value);
    
    public bool IsEndBattle => MaxTurn <= CrtTurn;
}
