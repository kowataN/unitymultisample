﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// バトル用プレイヤー
/// </summary>
public sealed class BattlePlayerModel
{
    /// <summary>
    /// HP
    /// </summary>
    private IntReactiveProperty _rxHP = new IntReactiveProperty();
    public IObservable<int> HPObservable => _rxHP.AsObservable();
    public int HP
    {
        get => _rxHP.Value;
        set => _rxHP.Value = value;
    }

    /// <summary>
    /// MP
    /// </summary>
    private IntReactiveProperty _rxMP = new IntReactiveProperty();
    public IObservable<int> MPObservable => _rxMP.AsObservable();
    public int MP
    {
        get => _rxMP.Value;
        set => _rxMP.Value = value;
    }

    private IntReactiveProperty _rxPoint = new IntReactiveProperty();
    public IObservable<int> PointObservable => _rxPoint.AsObservable();
    public int Point
    {
        get => _rxPoint.Value;
        set => _rxPoint.Value = value;
    }

    public int PlayerIndex => PlayerModel.PlayerIndex;

    public PlayerModel PlayerModel { get; set; } = new PlayerModel(-1);

    /// <summary>
    /// プレイヤータイプ
    /// </summary>
    public PlayerType PlayerType { get; set; } = PlayerType.CPU;

    private playerControl _playerControl = default;

    /// <summary>
    /// 対象の位置
    /// </summary>
    private Transform _targetTransform = default;
    public Transform TargetTransform
    {
        get => _targetTransform;
        set => _targetTransform = value;
    }

    private AbnormalType _abnormal = AbnormalType.Normal;
    public AbnormalType Abnormal => _abnormal;

    public BattlePlayerModel() {}

    public void SetPlayerControl(playerControl ctrl)
    {
        _playerControl = ctrl;
        _playerControl.OnEndCallback.AddListener(EndAnimeCallback);
    }

    public UnitModel UnitModel => PlayerModel.UnitModel;

    public List<SkillModel> SkillList => UnitModel.SkillList;

    public Attributes UnitAttribute => PlayerModel.UnitModel.UnitStatus.Atr;

    public void AddHitPoint(int value) =>_rxHP.Value += value;

    public bool IsStun => _rxHP.Value <= 0 || _abnormal.Equals(AbnormalType.Stun);
    
    public void AddMagicPoint(int value)
    {
        var maxMP = UnitModel.UnitStatus.Status.MP;
        var newMP = MP + value;
        MP = Mathf.Clamp(newMP, 0, maxMP);
    }

    public void SetAbnormalStatus(AbnormalType type) =>_abnormal = type;

    private enum Motion
    {
        Idle, Atk, Def, Stun, Stunning, GitHit
    }
    private Motion _motion = Motion.Idle;

    private Action _endCallback = default;

    public void PlayUnitAnimation(UnitMotionType type, Action endCallback = null)
    {
        _endCallback = endCallback;

        switch (type)
        {
            case UnitMotionType.Idle:
                _motion = Motion.Idle;
                _playerControl.Idle();
                break;
            case UnitMotionType.Defense:
                _motion = Motion.Def;
                _playerControl.Defend();
                break;
            case UnitMotionType.Attack:
                _motion = Motion.Atk;
                _playerControl.BasicAttack();
                break;
            case UnitMotionType.Magic:
                _motion = Motion.Atk;
                _playerControl.FlameAttack();
                break;
            case UnitMotionType.GetHit:
                _motion = Motion.GitHit;
                _playerControl.GetHit();
                break;
            case UnitMotionType.Stunning:
                _motion = Motion.Stunning;
                _playerControl.Stunning();
                break;
            default:
                _motion = Motion.Stun;
                _playerControl.Stun();
                break;
        }
    }

    private void EndAnimeCallback()
    {
        if (_motion == Motion.Stun || _motion == Motion.Stunning)
        {
            _playerControl.Stunning();
        }

        _endCallback?.Invoke();
    }
}
