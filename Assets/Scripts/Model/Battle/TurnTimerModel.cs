using System;
using UniRx;

public class TurnTimerModel {
    private FloatReactiveProperty _rpTimer = new FloatReactiveProperty();
    public IObservable<float> TimerObservable { get { return _rpTimer.AsObservable(); } }

    public float NowTimer => _rpTimer.Value;

    public void ResetTimer(float value) => _rpTimer.Value = value;
    public void UpdateTimer(float deltaTime) => _rpTimer.Value -= deltaTime; 
}
