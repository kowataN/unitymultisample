﻿public class EnemyModel
{
    public int EnemyNo = 0;
    public UnitModel Unit
    {
        get; set;
    }

    public EnemyModel()
    {
    }

    public EnemyModel(EnemyModel model)
    {
        if (model == null)
        {
            return;
        }

        this.EnemyNo = model.EnemyNo;
        this.Unit = model.Unit;
    }
}
