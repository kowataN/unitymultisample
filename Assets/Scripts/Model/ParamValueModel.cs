﻿using System;
using UniRx;
using UnityEngine;

public sealed class ParamValueModel
{
    private IntReactiveProperty _rp = default;
    private int _initialValue;
    private int _maxValue;

    public ParamValueModel(int initial, int max)
    {
        _initialValue = initial;
        _maxValue = max;

        _rp = new IntReactiveProperty(_initialValue);
    }

    public IObservable<int> Observable => _rp.AsObservable();

    public int Value
    {
        get => _rp.Value;
        private set => _rp.Value = value;
    }

    /// <summary>
    /// 値を設定します
    /// </summary>
    /// <param name="value"></param>
    public void SetValue(int value) => Value = Mathf.Clamp(value, 0, _maxValue);

    /// <summary>
    /// 現在の値に加算します
    /// </summary>
    /// <param name="value"></param>
    public void AddValue(int value) => SetValue(Value + value);
}
