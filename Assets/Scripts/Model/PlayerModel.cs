﻿using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public sealed class PlayerModel
{
    private PlayerInfo _player;

    /// <summary>
    /// プレイヤーインデックス
    /// </summary>
    private int _playerIndex = -1;
    public int PlayerIndex => _playerIndex;

    /// <summary>
    /// プレイヤーデータを作成します。
    /// </summary>
    public PlayerModel(int index)
    {
        _player = new PlayerInfo();
        _playerIndex = index;
    }

    /// <summary>
    /// プレイヤー情報を作成します。
    /// </summary>
    /// <returns>プレイヤー情報</returns>
    /// <param name="name">プレイヤー名</param>
    /// <param name="unitNo">使用ユニットNO</param>
    /// <param name="addStatus">加算分のパラメータ</param>
    public static PlayerModel Create(int index, string name, int unitNo)
    {
        PlayerModel instance = new PlayerModel(index);
        instance.SetPlayerData(name);
        UnitModel unit = UnitStorage.Inst.GetDataFromNo(unitNo);
        if (unit == null)
        {
            DebugLogger.LogError($"IS NOT UnitModel No[{unitNo}]");
        }
        instance.SetUnitData(unit);
        return instance;
    }

    /// <summary>
    /// プレイヤー情報を設定します。
    /// </summary>
    /// <param name="name">プレイヤー名</param>
    public void SetPlayerData(string name)
    {
        // TODO: 必要な情報を足していく
        _player.Name = name;
    }

    /// <summary>
    /// ユニット情報を設定します。
    /// </summary>
    /// <param name="unit">ユニット情報</param>
    /// <param name="addStatus">ステータス加算値情報</param>
    public void SetUnitData(UnitModel unit) => _player.UnitModel = unit;

    /// <summary>
    /// ユニット情報を返します。
    /// </summary>
    /// <returns>ユニット情報</returns>
    public UnitModel UnitModel => _player.UnitModel;

    public string PlayerName => _player.Name ?? "";
    
    /// <summary>
    /// 使用可能なスキル一覧を返します。
    /// </summary>
    /// <returns></returns>
    public IEnumerable<SkillModel> GetAvailableSkills()
    {
        int nowCost = _player.UnitModel.UnitStatus.Status.MP;
        return _player.UnitModel.SkillList.Where(x => x.Cost <= nowCost);
    }
}
