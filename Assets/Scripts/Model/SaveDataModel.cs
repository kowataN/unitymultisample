﻿using System;
using UnityEngine;

[Serializable]
public sealed class SaveDataModel
{
    /// <summary>
    /// プレイヤー情報
    /// </summary>
    public PlayerModel PlayerModel
    {
        get; set;
    }

    /// <summary>
    /// セーブデータのインスタンス
    /// </summary>
    private static SaveDataModel _Instance;
    public static SaveDataModel Instance => _Instance ?? (_Instance = new SaveDataModel());

    private SaveDataModel()
    {
        PlayerModel = new PlayerModel(-1);
    }


    /// <summary>
    /// プレイヤー情報を初期化します
    /// </summary>
    private void Init()
    {
        if (Load() == false)
        {
            // セーブデータの読み込みに失敗したので初期化
            SetPlayerData("名無し");
        }
    }

    /// <summary>
    /// セーブデータを保存します
    /// </summary>
    public bool Save()
    {
        string json = JsonUtility.ToJson(PlayerModel);
        return FileIO.SaveJsonData(Utility.Utility.GetSaveDataFullPath(), json);
    }

    /// <summary>
    /// セーブデータを読み込みます
    /// </summary>
    /// <returns>The load.</returns>
    public bool Load()
    {
        bool ret = true;
        string savedata = FileIO.Load(Utility.Utility.GetSaveDataFullPath());
        if (string.IsNullOrEmpty(savedata))
        {
            DebugLogger.LogError("savedata.binが存在しませんでした。");
            ret = false;
        }
        else
        {
            DebugLogger.Log("LoadData : " + savedata);
            PlayerModel = JsonUtility.FromJson<PlayerModel>(savedata);
        }
        return ret;
    }

    /// <summary>
    /// プレイヤー情報を設定します
    /// </summary>
    /// <param name="name">プレイヤー名</param>
    public void SetPlayerData(string name) => PlayerModel.SetPlayerData(name);

    /// <summary>
    /// ユニット情報を設定します
    /// </summary>
    /// <param name="unit">ユニット情報</param>
    public void SetUnitData(UnitModel unit) => PlayerModel.SetUnitData(unit);
}
