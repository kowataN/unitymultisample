﻿using System.Collections.Generic;


[System.Serializable]
public class OptionalList
{
    public List<OptionalInfo> Optional;
}

/// <summary>スキルモデル</summary>
[System.SerializableAttribute]
public struct SkillModel
{
    /// <summary>番号</summary>
    public int No;

    /// <summary>属性</summary>
    public Attributes Atr;

    /// <summary>スキルタイプ</summary>
    public SkillTypes Type;

    /// <summary>スキルランク</summary>
    public int Rank;

    /// <summary>行動速度</summary>
    public int Speed;

    /// <summary>コスト</summary>
    public int Cost;

    /// <summary>名前</summary>
    public string Name;

    /// <summary>コメント</summary>
    public string Comment;

    /// <summary>対象</summary>
    public SkillTargetTypes Target;

    /// <summary>対象人数</summary>
    public int Member;

    /// <summary>継続ターン</summary>
    public int Turn;

    /// <summary>効果値</summary>
    public int Damage;

    /// <summary>モーション情報</summary>
    public string Motion;

    /// <summary>オプション情報一覧</summary>
    public OptionalList OptionalList;

    /*
            public SkillModel(int no) {
                this.No = no;
                this.Atr = Attributes.None;
                this.Type = SkillTypes.None;
                this.Rank = 0;
                this.Speed = 0;
                this.Cost = 0;
                this.Name = "なし";
                this.Comment = "なし";
                this.Target = SkillTargetTypes.Enemy;
                this.Member = 1;
                this.Turn = 1;
                this.Damage = 0;
                this.Motion = "";
                this.OptionalList = new OptionalList();
            }
    */
}
