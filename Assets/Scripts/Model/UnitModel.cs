﻿using System.Collections.Generic;
using UnityEngine;

public sealed class UnitModel
{
    private UnitStatus _unitStatus = new UnitStatus {
        No = -1,
        Atr = Attributes.None,
        Tribe = Tribes.None,
        //Status.ZeroClear(),
        SkillSetNo = 1,
        Use = false,
        Name = ""
    };

    public UnitStatus UnitStatus => _unitStatus;

    /// <summary>
    /// ユニットの所持スキル情報
    /// </summary>
    private List<SkillModel> _skillList = new List<SkillModel>();
    public List<SkillModel> SkillList => _skillList;

    public UnitModel()
    {
    }

    public UnitModel(UnitModel unitModel)
    {
        if (unitModel == null)
        {
            return;
        }
        this._unitStatus = new UnitStatus(unitModel.UnitStatus);
        this._skillList.Clear();
        unitModel.SkillList.ForEach(x => this._skillList.Add(x));
    }

    public void SetParameter(int number,
                             Attributes atr,
                             Tribes tribe,
                             BasicStatus staus,
                             int skillSet,
                             string name)
    {
        _unitStatus.No = number;
        _unitStatus.Atr = atr;
        _unitStatus.Tribe = tribe;
        _unitStatus.Status = staus;
        _unitStatus.SkillSetNo = skillSet;
        _unitStatus.Name = name;
    }

    public void SetSkillSet(int skillSet)
    {
        _unitStatus.SkillSetNo = skillSet;
        _skillList.Clear();
        var skillset = SkillSetStorage.Inst.GetSkillSet(skillSet);
        skillset.ForEach(x => _skillList.Add(x));
    }

    public int[] GetParamterArray()
    {
        return new int[] {
            _unitStatus.Status.HP,
            _unitStatus.Status.MP,
            _unitStatus.Status.Attack,
            _unitStatus.Status.Magic,
            _unitStatus.Status.Defense,
            _unitStatus.Status.Luck
        };
    }

    public void AddHP(int value)
    {
        _unitStatus.Status.HP = Mathf.Max(_unitStatus.Status.HP + value, 0);
    }
}
