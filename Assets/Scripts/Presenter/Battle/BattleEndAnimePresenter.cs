using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BattleEndAnimePresenter
{
    private BattleEndAnimeView _view = default;

    public UnityAction EndAnimeCallback;

    public void SetUp(BattleEndAnimeView view)
    {
        _view = view;
        _view.gameObject.SetActive(false);
        _view.EndAnimeCallback = () => {
            EndAnimeCallback?.Invoke();
        };
    }

    public void Play(UnityAction action)
    {
        _view.gameObject.SetActive(true);
        EndAnimeCallback = action;
        _view.Play();
    }
}
