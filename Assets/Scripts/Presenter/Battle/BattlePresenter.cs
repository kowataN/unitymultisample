using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using static BattleView;

public sealed class BattlePresenter : MonoBehaviour
{
    [SerializeField]
    private BattleData _battleData = default;

    private BattleModel _battleModel = default;
    private BattleView _battleView = default;
    public BattleView BattleView => _battleView;

    private Dictionary<string, GameObject> _playerPrefas = new Dictionary<string, GameObject>();

    /// <summary>
    /// 現在のターン数
    /// </summary>
    public int CrtTurn => _battleModel.CrtTurn;

    /// <summary>
    /// 行動管理
    /// </summary>
    private ActionManager _actionManager = new ActionManager(GameTypes.MaxBattlePlayerNumber);
    public ActionManager ActionManager => _actionManager;

    /// <summary>
    /// AI
    /// </summary>
    private List<Thinker> _thinker;

    // スキル情報パネル
    private SkillPanelPresenter _skillPanelPresenter = default;
    // スキルアイコン情報
    private SkillIconPresenter[] _skillIconPresenters = default;

    // シールド
    [SerializeField] private ShieldPresenter[] _shieldPresenters = default;

    // タイマー
    private TurnTimerPresenter _turnTiemrPresenter = default;
    public float NowTimer => _turnTiemrPresenter.NowTimer;

    // ステータス情報
    private Dictionary<int, StatusPresenter> _statusPresenters = new Dictionary<int, StatusPresenter>();
    public StatusPresenter GetStatusPresenter(int index) => _statusPresenters[index];

    // 向き情報
    private RotateInfo[] _rotateInfos = new RotateInfo[GameTypes.MaxBattlePlayerNumber];

    [SerializeField] private ParamValuePresenter _damageValuePresenter = default;

    private TurnStartAnimePresenter _turnStartAnimePresenter = default;
    public TurnStartAnimePresenter TurnStartAnime => _turnStartAnimePresenter;

    private BattleEndAnimePresenter _gameEndAnimePresenter = default;
    public BattleEndAnimePresenter GameEndAnime => _gameEndAnimePresenter;

    public int MyIndex
    {
        get; set;
    }

    // デバッグ機能
    public bool IsStateStop { get; set; } = false;

    public bool IsTouchUnitEnabled { get; set; } = false;

    public void SetUp()
    {
        _battleView = GetComponentInChildren<BattleView>();

        for (int i = 0; i < _rotateInfos.Length; ++i)
        {
            _rotateInfos[i] = new RotateInfo();
        }

        //_unitAnimator = new Animator[GameTypes.MaxBattlePlayerNumber];

        _battleModel = BattleModelFactory.CreateBattleModel(_battleView, _battleData);

        // プレイヤー、CPUを作成する
        CreateBattlePlayer();

        _actionManager.Init(this);

        for (int i = 0; i < _battleModel.BattlePlayers.Count; ++i)
        {
            CreateUnitPrefab(i, _battleView.UnitNodes[i].transform);
        }

        // スキルアイコン表示
        _battleView.CreateSkillIconList(_battleModel.BattlePlayers[0].PlayerModel.UnitModel.SkillList);

        InitStatusPresenter();
        InitTurnTimerPresenter();
        InitSkillPanelPresenter();
        InitSkillIconPresenter();

        _battleView.SetUnitCamera(MyIndex);

        // メッセージを非表示にする
        _battleView.ChangeDisplayUI(BattleView.DisplayMode.Skill);

        // ダメージ表記を一旦けす
        _damageValuePresenter.SetVisible(false);

        // ターン開始アニメ
        _turnStartAnimePresenter = new TurnStartAnimePresenter();
        _turnStartAnimePresenter.SetUp(_battleView.TurnStartAnimeView, _battleModel.MaxTurn);

        // ゲーム終了アニメ
        _gameEndAnimePresenter = new BattleEndAnimePresenter();
        _gameEndAnimePresenter.SetUp(_battleView.GameEndAnimeView);

        // debug
        DebugPointAnime.Inst.Presenter = this;
    }

    private void CreateBattlePlayer()
    {
        // プレイヤー作成
        _battleModel.CreatePlayer(0, "プレイヤー1", 11);

        // CPU作成
        CreateCpu();
    }

    /// <summary>
    /// CPUを追加します
    /// </summary>
    /// <param name="count">追加数</param>
    private void CreateCpu()
    {
        _battleModel.CreateCpu();

        int number = _battleModel.CpuNumber;
        _thinker = new List<Thinker>(number);
        for (int i = 0; i < _battleModel.BattlePlayers.Count; ++i)
        {
            if (_battleModel.BattlePlayers[i].PlayerType == PlayerType.CPU)
            {
                _thinker.Add(new Thinker(_battleModel.BattlePlayers[i].PlayerModel.PlayerIndex, _battleModel.BattlePlayers));
            }
        }
    }

    /// <summary>
    /// ユニットのオブジェクト作成する
    /// </summary>
    /// <param name="index"></param>
    /// <param name="transform"></param>
    public void CreateUnitPrefab(int index, Transform transform)
    {
        BattlePlayerModel player = GetPlayerModel(index);

        // ユニット追加
        var prefab = PrefabCreator.CreateUnit(player.UnitModel.UnitStatus.Atr, transform);
        if (prefab == null)
        {
            return;
        }

        player.SetPlayerControl(prefab.GetComponentInChildren<playerControl>());

        prefab.tag = "Player" + (index + 1).ToString();
        _playerPrefas.Add(prefab.tag, prefab);

        var src = prefab.GetComponentInChildren<PickupObj>();
        if (src)
        {
            src.SetIndex(index);
            // タップしたプレイヤーの方向を向く
            src.OnClickAction += (tag, targetIndex) => {
                if (!IsTouchUnitEnabled)
                {
                    return;
                }

                // プレイヤーの対象を設定する
                var actInfo = _actionManager.GetActionInfo(0);
                actInfo.TargetIndex = targetIndex;
                _actionManager.SetActionInfo(actInfo);
                LookAtTargetPlayer(0, index);

                _battleView.UnitSelectMarkerView.SetPosition(targetIndex);
            };
        }

        // 自身の当たり判定を消す
        if (index == 0)
        {
            var boxCollier = prefab.GetComponentInChildren<BoxCollider>();
            if (boxCollier)
            {
                boxCollier.enabled = false;
            }
        }
    }

    public void LookAtTargetPlayer(int index, int targetIndex)
    {
        // このタイミングで向き先を決定する
        float angle = GameTypes.RotateTbl[index][targetIndex];
        _rotateInfos[index].TargetRot = Quaternion.Euler(0f, angle, 0f);
    }

    public void UpdateUnitRotate()
    {
        int loopCt = _battleView.PlayerNodes.Length;
        for (int i = 0; i < loopCt; ++i)
        {
            UpdateUnitRotate(i, 1f);
        }
    }

    private void UpdateUnitRotate(int index, float time)
    {
        _battleView.PlayerNodes[index].transform.rotation =
                Quaternion.RotateTowards(
                    _battleView.PlayerNodes[index].transform.rotation,
                    _rotateInfos[index].TargetRot,
                    time);
    }

    public IEnumerator UpdateTargetRotate(int index, int target)
    {
        // 自身と対象の向きを変える
        int endCount = 0;
        void callback() => endCount++;
        ExecRotate(index, target);
        yield return new WaitUntil(() => endCount == 2);

        // 自身をtargetの方へ向ける
        void ExecRotate(int myIndex, int target)
        {
            StartCoroutine(RotateUnit(myIndex, target, callback));
            StartCoroutine(RotateUnit(target, myIndex, callback));
        }
    }

    private IEnumerator RotateUnit(int myIndex, int target, Action callback)
    {
        if (myIndex == 3)
        {
            int i = 0;
        }

        float angle = GameTypes.RotateTbl[myIndex][target];
        float crtAngle = _battleView.PlayerNodes[myIndex].transform.localEulerAngles.y;
        if (angle == crtAngle)
        {
            callback?.Invoke();
            yield break;
        }

        int addValue = angle < crtAngle ? -1 : 1;
        angle = Mathf.Abs(angle - crtAngle);

        angle = Mathf.Abs(angle);
        for (int turn = 0; turn < angle; turn++)
        {
            _battleView.PlayerNodes[myIndex].transform.Rotate(0, addValue, 0);
            yield return new WaitForSeconds(0.00f);
        }
        callback?.Invoke();
    }

    /// <summary>
    /// CPUの行動決定
    /// </summary>
    public void Think()
    {
        foreach (Thinker think in _thinker)
        {
            if (_battleModel.BattlePlayers[think.MyIndex].IsStun)
            {
                continue;
            }

            var resultThink = think.Think();
            LookAtTargetPlayer(think.MyIndex, resultThink.TargetIndex);
            _actionManager.SetActionInfo(resultThink);
        }
    }

    public bool CheckAction() => _actionManager.BaseActionInfos.Length == GameTypes.MaxBattlePlayerNumber;

    public void SetActionOrder() => _actionManager.SetActionOrder();

    /// <summary>
    /// 行動順を返します
    /// </summary>
    /// <returns></returns>
    public List<ActionInfo> ActionInfo => _actionManager.OrderList;

    public void UpdateTimer() => _turnTiemrPresenter.TurnTimerModel.UpdateTimer(Time.deltaTime);
    private void ResetTiemr() => _turnTiemrPresenter.TurnTimerModel.ResetTimer(_battleModel.InputLatency);

    private void InitStatusPresenter()
    {
        for (int i = 0; i < _battleView.UnitStatusView.Length; i++)
        {
            StatusPresenter sp = new StatusPresenter(_battleModel.BattlePlayers[i], _battleView.UnitStatusView[i]);
            _statusPresenters.Add(i, sp);
        }
    }

    private void InitTurnTimerPresenter()
    {
        _turnTiemrPresenter = new TurnTimerPresenter(_battleView.TurnTimerView);
        _turnTiemrPresenter.Init(_battleModel.InputLatency);
    }

    private void InitSkillPanelPresenter()
    {
        _skillPanelPresenter = new SkillPanelPresenter(_battleView.SkillPanelView);
    }

    private void InitSkillIconPresenter()
    {
        _skillIconPresenters = new SkillIconPresenter[_battleView.SkillIconList.Length];
        int index = 0;
        foreach (PartsSkillIconView iconView in _battleView.SkillIconList)
        {
            _skillIconPresenters[index] = new SkillIconPresenter(iconView);
            _skillIconPresenters[index].SetClickObservable();
            _skillIconPresenters[index].OnClickSkill += (idx) => {
                if (!IsTouchUnitEnabled)
                {
                    return;
                }

                _battleView.SkillSelectMarker.transform.SetParent(_skillIconPresenters[idx].ViewObject.transform);
                _battleView.SkillSelectMarker.transform.localPosition = Vector3.zero;
                _battleView.SkillSelectMarker.SetActive(true);

                // 選択したスキル情報を画面に反映
                var skillList = _battleModel.BattlePlayers[0].UnitModel.SkillList;
                var skillModel = skillList[idx];
                _skillPanelPresenter?.UpdateSkillDisply(skillModel);

                // プレイヤーのスキルを設定する
                var actInfo = _actionManager.GetActionInfo(0);
                actInfo.Skill = skillModel;
                _actionManager.SetActionInfo(actInfo);
            };
            index++;
        }
    }

    public void ChangeDisplayUI(DisplayMode mode) => _battleView.ChangeDisplayUI(mode);

    public void SetBattleMessage(string msg, bool lastWait)
    {
        _battleView.MessageView.ResetMessage();
        DebugLogger.Log(msg);
        base.StartCoroutine(_battleView.MessageView.TypeMessage(msg));
    }

    public BattlePlayerModel GetPlayerModel(int playerIndex)
    {
        return _battleModel.BattlePlayers.Where(x => x.PlayerIndex == playerIndex).FirstOrDefault();
    }

    public void SetMainCameraPos(int index) => _battleView.SetMainCameraPos(index);

    /// <summary>
    /// 指定プレイヤーのシールド表示状態を設定します。
    /// </summary>
    /// <param name="index"></param>
    /// <param name="skill"></param>
    public void SetDisplayShield(int index, bool value)
    {
        if (index >= _shieldPresenters.Length || index < 0)
        {
            return;
        }
        _shieldPresenters[index].SetDisplay(value);
    }

    public bool IsDisplayShield(int index)
    {
        if (index >= _shieldPresenters.Length || index < 0)
        {
            return false;
        }
        return _shieldPresenters[index].gameObject.activeSelf;
    }

    /// <summary>
    /// バトルが終了したかを返します。
    /// </summary>
    /// <returns></returns>
    public bool IsEndBattle => _battleModel.IsEndBattle;

    /// <summary>
    /// MP回復量
    /// </summary>
    public int RecoveryMPValue => _battleModel.RecoveryMPValue;

    /// <summary>
    /// ターンを加算します。
    /// </summary>
    public void AddTurn() => _battleModel.AddTurn();

    /// <summary>
    /// 指定プレイヤーが気絶しているかを返します。
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    public bool IsStun(int index) => _battleModel.BattlePlayers[index].IsStun;

    /// <summary>
    /// すべてのプレイヤーのMPを回復します。
    /// </summary>
    public void RecoveryMPAllPlayer()
    {
        _battleModel.BattlePlayers.ToList().ForEach(player => {
            if (!player.IsStun)
            {
                player.AddMagicPoint(_battleModel.RecoveryMPValue);
            }
        });
    }

    public void InitAtTurnStart()
    {
        ResetTiemr();

        _thinker.ForEach(x => x.InitAtTurnStart());
    }

    public int GetPlayerCrtHP(int index) => _battleModel.BattlePlayers[index].HP;
    public int GetPlayerCrtPoint(int index) => _battleModel.BattlePlayers[index].Point;

    public void ShowDmageValue(int index, int addValue)
    {
        _damageValuePresenter.ShowAddAnime(index, addValue, null);
    }

    public void UpdateRanking()
    {
        Dictionary<int, List<int>> rankingDict = new Dictionary<int, List<int>>();

        foreach (var player in _battleModel.BattlePlayers)
        {
            if (!rankingDict.ContainsKey(player.Point))
            {
                rankingDict.Add(player.Point, new List<int>());
            }
            rankingDict[player.Point].Add(player.PlayerIndex);
        }

        var sorted = rankingDict.OrderByDescending(x => x.Key);

        int ranking = 1;
        foreach (var value in sorted)
        {
            foreach (var idx in value.Value)
            {
                _statusPresenters[idx].UpdateRanking(ranking);
            }
            ranking++;
        }
    }

    public void PlayBattleEnd(UnityAction callback) => _gameEndAnimePresenter.Play(callback);

    public void SaveBattleResult()
    {
        BattleResultCache.Save(this);
    }
}
