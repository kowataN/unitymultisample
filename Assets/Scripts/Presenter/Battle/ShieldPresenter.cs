using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class ShieldPresenter : MonoBehaviour
{
    public System.Action<string> OnEndDisplayAnime;
    [SerializeField] ShieldView _view = default;
    [SerializeField] Animation _animation = default;

    public void SetDisplay(bool value)
    {
        gameObject.SetActive(value);
        _view.gameObject.SetActive(value);
        //_animation.Play("shield");
    }

    public void EndDisplayAnime()
    {
        Debug.Log("ShieldPresenter::EndDisplayAnime");
    }
}
