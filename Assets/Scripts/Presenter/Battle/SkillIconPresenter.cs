﻿using System;
using UnityEngine;

public sealed class SkillIconPresenter
{
    private SkillModel _skillModel = default;
    public SkillModel SkillModel => _skillModel;

    private PartsSkillIconView _view = default;
    public GameObject ViewObject => _view.gameObject;

    public Action<int> OnClickSkill;

    public SkillIconPresenter(PartsSkillIconView view) => _view = view;

    public void SetClickObservable() => _view.Button.onClick.AddListener(OnClickButton);

    private void OnClickButton() => OnClickSkill?.Invoke(_view.Index);
}
