﻿public sealed class SkillPanelPresenter
{
    private SkillPanelView _view = default;

    public SkillPanelPresenter(SkillPanelView view)
    {
        _view = view;
    }

    public void UpdateSkillDisply(SkillModel? skill) => _view.UpdateSkillDisply(skill);
}

