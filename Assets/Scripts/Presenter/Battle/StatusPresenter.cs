﻿using System.Collections;
using UniRx;
using UnityEngine;

public sealed class StatusPresenter : IPresenter
{
    private BattlePlayerModel _player = default;
    private StatusView _view = default;

    public System.Action Callback = default;

    public StatusPresenter(BattlePlayerModel player, StatusView view)
    {
        _player = player;
        _view = view;

        _view.SetData(_player, 1);
        SetObservable();
    }

    private void SetObservable()
    {
        _player.HPObservable
            .SubscribeToText(_view.HP)
            .AddTo(_view.gameObject);

        _player.MPObservable
            .SubscribeToText(_view.MP)
            .AddTo(_view.gameObject);

        _player.PointObservable
            .SubscribeToText(_view.Point)
            .AddTo(_view.gameObject);
    }

    public IEnumerator AddHPAnime(int addValue, float time)
    {
        int oldValue = _player.HP;
        int newValue = Mathf.Max(oldValue + addValue, 0);
        float deltaTime = 0.0f;

        while (deltaTime < time)
        {
            float rate = deltaTime / time;

            _player.HP = Mathf.Max((int)(oldValue + addValue * rate), 0);
            if (_player.IsStun)
            {
                deltaTime = time;
            }

            deltaTime += Time.deltaTime;
            // 0.01秒待つ
            yield return new WaitForSeconds(0.001f);
        }

        _player.HP = newValue;
    }

    public IEnumerator AddMPAnime(int addValue, float time)
    {
        int oldValue = _player.MP;
        int newValue = oldValue + addValue;
        float deltaTime = 0.0f;

        while (deltaTime < time)
        {
            float rate = deltaTime / time;

            _player.MP = (int)(oldValue + (addValue) * rate);

            deltaTime += Time.deltaTime;
            // 0.01秒待つ
            yield return new WaitForSeconds(0.001f);
        }

        _player.MP = newValue;
    }

    public IEnumerator AddPointAnime(int addValue, float time)
    {
        int oldValue = _player.Point;
        int newValue = oldValue + addValue;
        float deltaTime = 0.0f;

        while (deltaTime < time)
        {
            float rate = deltaTime / time;

            _player.Point = (int)(oldValue + (addValue) * rate);

            deltaTime += Time.deltaTime;
            // 0.01秒待つ
            yield return new WaitForSeconds(0.001f);
        }

        _player.Point = newValue;
    }

    public void UpdateRanking(int ranking)
    {
        _view.SetRanking(ranking);
    }

    public int GetRanking()
    {
        return _view.Ranking;
    }
}
