using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TurnStartAnimePresenter
{
    private TurnStartAnimeView _view = default;

    public UnityAction EndAnimeCallback;

    public void SetUp(TurnStartAnimeView view, in int maxTurn)
    {
        _view = view;
        _view.gameObject.SetActive(false);
        _view.SetUp(1, maxTurn);
        _view.EndAnimeCallback = () => {
            _view.gameObject.SetActive(false);
            EndAnimeCallback?.Invoke();
        };
    }

    public void Play(in int turn)
    {
        _view.gameObject.SetActive(true);
        _view.Play(turn);
    }
}
