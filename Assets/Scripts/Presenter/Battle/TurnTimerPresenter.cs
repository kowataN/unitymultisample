using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class TurnTimerPresenter : IPresenter
{
    private TurnTimerModel _turnTimerModel = new TurnTimerModel();
    private TurnTimerView _view = default;

    public TurnTimerModel TurnTimerModel => _turnTimerModel;

    public float NowTimer => _turnTimerModel.NowTimer;

    public TurnTimerPresenter(TurnTimerView view)
    {
        _view = view;

        SetObservable();
    }

    public void SetObservable()
    {
        _turnTimerModel.TimerObservable
            .Subscribe(x => _view.SetNowTimer(x))
            .AddTo(_view.gameObject);
    }

    public void Init(float timerValue)
    {
        _turnTimerModel.ResetTimer(timerValue);
        _view.SetMaxTimerValue(timerValue);
    }
}
