using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


public class BattleResultPresenter : MonoBehaviour
{
    [SerializeField] private BattleResultView _resultView = default;
    [SerializeField] private BattleResultCacheInfo _saveData = default;

    private void Start()
    {
        SetUp();
    }

    private void EndShowRanking()
    {
        BattleResultCache.Delete();
    }

    private void SetUp()
    {
        Time.timeScale = 1.0f;

        _saveData = BattleResultCache.Load();
        SetPlayerInfo();

        _resultView.SetUp(EndShowRanking);
        _resultView.FadeIn();
    }

    private void SetPlayerInfo()
    {
        Dictionary<int, List<BattleResultCacheData>> rankingDict =
            new Dictionary<int, List<BattleResultCacheData>>();

        foreach (var player in _saveData.List)
        {
            if (!rankingDict.ContainsKey(player.Point))
            {
                rankingDict.Add(player.Point, new List<BattleResultCacheData>());
            }
            rankingDict[player.Point].Add(player);
        }

        var sorted = _saveData.List.OrderByDescending(x => x.Ranking);

        int index = 0;
        foreach (var value in sorted)
        {
            _resultView.SetInfoParts(index++, value);
        }
    }
}
