﻿using DG.Tweening;
using UniRx;
using UnityEngine;

public class ParamValuePresenter : MonoBehaviour
{
    [SerializeField] private Transform[] _parents = default;
    [SerializeField] private ParamValueView _view = default;

    public void SetVisible(bool value) => _view.gameObject.SetActive(value);

    public void ShowAddAnime(int index, int addValue, TweenCallback callback)
    {
        _view.gameObject.transform.SetParent(_parents[index].gameObject.transform);
        _view.gameObject.transform.localPosition = Vector3.zero;

        _view.ShowAddAnime(addValue, callback);
    }
}
