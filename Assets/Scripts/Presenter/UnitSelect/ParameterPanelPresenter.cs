using System;
using UniRx;

public class ParameterPanelPresenter
{
    private ParameterPanelView _view = null;

    public IObservable<Unit> OnClickBack => _view.OnClickBack;
    public IObservable<Unit> OnClickSkill => _view.OnClickSkill;

    public void Setup(ParameterPanelView view)
    {
        _view = view;
    }
}
