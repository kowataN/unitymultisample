using System;
using System.Collections.Generic;
using UniRx;
using System.Linq;

public class SkillSelectPresenter
{
    private SkillSelectView _view = null;
    private int selectCt = 0;

    public IObservable<Unit> OnClickBack => _view.OnClickBack;
    public IObservable<Unit> OnClickStart => _view.OnClickStart;

    public void Setup(SkillSelectView view)
    {
        _view = view;
        _view.SetOnSelected(OnSelect);
    }

    public void SetSkillList(int index)
    {
        var unitModel = UnitStorage.Inst.GetDataFromIndex(index);
        _view.SetSkillList(unitModel);
    }

    public void OnSelect()
    {
        var list = _view.PartsList;
        int count = 0;
        foreach (var parts in list)
        {
            count += parts.Selected ? 1 : 0;
        }

        foreach (var parts in list)
        {
            parts.SelectInteractable = (count == 6) ? parts.Selected : true;
        }

        _view.SelectCount = count;
    }

    public List<int> GetSkillList()
    {
        return _view.PartsList
            .Where(x => x.Selected)
            .Select(x => x.SkillNo)
            .ToList();
    }
}
