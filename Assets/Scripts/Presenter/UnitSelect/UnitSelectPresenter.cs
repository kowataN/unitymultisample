using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UnitSelectPresenter : MonoBehaviour
{
    [SerializeField] private UnitSelectView _view = null;
    private UnitModel _crtUnit = default;

    private ParameterPanelPresenter _paramPresenter = null;
    private SkillSelectPresenter _skillSelectPresenter = null;

    private int _selectIndex = 0;
    private int _maxCharacter;

    private void Awake()
    {
        _maxCharacter = UnitStorage.Inst.UnitList.Count;
        _paramPresenter = new ParameterPanelPresenter();
        _skillSelectPresenter = new SkillSelectPresenter();
    }

    private void Start()
    {
        SetupThisView();
        SetupParameterPanel();
        SetupSkillPanel();
    }

    private void SetupThisView()
    {
        _view.Setup(_selectIndex, _maxCharacter);

        _view.OnClickLeft
            .Subscribe(_ => {
                _selectIndex = Mathf.Clamp(--_selectIndex, 0, _maxCharacter - 1);
                _view.ChangeCharacter(_selectIndex);
            })
            .AddTo(_view.gameObject);

        _view.OnClickRight
            .Subscribe(_ => {
                _selectIndex = Mathf.Clamp(++_selectIndex, 0, _maxCharacter - 1);
                _view.ChangeCharacter(_selectIndex);
            })
            .AddTo(_view.gameObject);
    }

    private void SetupParameterPanel()
    {
        _paramPresenter.Setup(_view.ParameterView);

        _paramPresenter.OnClickBack
            .Subscribe(_ => {
                // TODO: 
            })
            .AddTo(_view.gameObject);

        _paramPresenter.OnClickSkill
            .Subscribe(_ => {
                _skillSelectPresenter.SetSkillList(_selectIndex);
                _view.ChangeView(UnitSelectView.ViewMode.SkillSelect);
            })
            .AddTo(_view.gameObject);
    }

    private void SetupSkillPanel()
    {
        _skillSelectPresenter.Setup(_view.SkillSelectView);

        _skillSelectPresenter.OnClickBack
            .Subscribe(_ => {
                _view.ChangeView(UnitSelectView.ViewMode.UnitSelect);
            })
            .AddTo(_view.gameObject);

        _skillSelectPresenter.OnClickStart
            .Subscribe(_ => {
                var skillList = _skillSelectPresenter.GetSkillList();
                SaveDataModel.Instance.SetUnitData(UnitStorage.Inst.GetDataFromIndex(_selectIndex)/*, addstatus*/);
                //SceneManager.LoadScene("MatchingRoom", LoadSceneMode.Single);
                SceneManager.LoadScene("Battle", LoadSceneMode.Single); // TODO: テスト
            })
            .AddTo(_view.gameObject);
    }
}
