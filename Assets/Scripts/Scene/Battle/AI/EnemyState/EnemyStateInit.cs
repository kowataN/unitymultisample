﻿using State;

public class EnemyStateInit : StateBase {
    private SubState _subState = default;

    public EnemyStateInit(executeState execute) : base(execute) {
        _subState = new SubState(ExecuteInit);
    }

    public override void Execute() {
        _subState?.Execute();
        ExecDelegate?.Invoke();
    }

    /// <summary>
    /// 各種初期化
    /// </summary>
    private void ExecuteInit() {
        _subState.ChangeState(ExecuteEnd);
    }

    private void ExecuteEnd() {
        //StateEnd = true;
        _subState.ChangeState(null);
    }
}
