﻿using State;


public class EnemyStateLoop : StateBase {
    private SubState _subState = default;

    //private EnemyPresenter _presenter;

    public EnemyStateLoop(executeState execute) : base(execute) {
        _subState = new SubState(ExecuteInit);
    }

    public void Init() {
        _subState.ChangeState(ExecuteInit);
    }

    public override void Execute() {
        //DebugView.Instance?.SetEnemySubStateString(_subState.GetStateName());
        _subState.Execute();
        if (ExecDelegate != null) {
            ExecDelegate();
        }
    }

    /// <summary>
    /// 各種初期化
    /// </summary>
    private void ExecuteInit() {
        //_presenter = (EnemyPresenter)BattleManager.Instance.EnemyPresenter;
        _subState.ChangeState(ExecuteLoop);
    }

    private void ExecuteLoop() {
    }

    private void EndCardMove() {
        //SoundManager.Instance.PlaySetCard();
        _subState.ChangeState(ExecuteLoop);
    }

    private void ExecuteOnClickDecideButton() {
        //_presenter.DecidePresenter.OnClickDecide();
        _subState.ChangeState(ExecuteEnd);
    }

    private void ExecuteEnd() {
        // = true;
    }
}
