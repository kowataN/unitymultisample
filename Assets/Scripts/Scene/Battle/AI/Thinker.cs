﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;


public class Thinker
{
    /// <summary>
    /// 自身の情報
    /// </summary>
    private BattlePlayerModel _my = default;

    /// <summary>
    /// プレイヤーインデックス
    /// </summary>
    public int MyIndex
    {
        get
        {
            return _my.PlayerIndex;
        }
    }

    /// <summary>
    /// ターゲット情報
    /// </summary>
    private BattlePlayerModel[] _enemy;

    private List<int> _targetIndex = new List<int>();

    private EnemyStateMain _state = new EnemyStateMain();

    public enum ThinkStatus
    {
        kIdle, kThink, kEnd
    };
    //private ReactiveProperty<ThinkStatus> _ThinkStatus = new ReactiveProperty<ThinkStatus>(ThinkStatus.kIdle);
    //public ReactiveProperty<ThinkStatus> RxThinkStatus => _ThinkStatus;

    public Thinker(int index, List<BattlePlayerModel> players)
    {
        _enemy = new BattlePlayerModel[players.Count - 1];

        int i = 0;
        foreach (var player in players)
        {
            if (player == null)
            {
                Debug.LogError("player is null");
                continue;
            }

            if (index == player.PlayerIndex)
            {
                _my = player;
                continue;
            }

            _enemy[i] = player;
            i++;
        }

        InitAtTurnStart();
    }

    /// <summary>
    /// ターン開始時に初期化する
    /// </summary>
    public void InitAtTurnStart()
    {
        _state.Init();

        // 気絶ユニットを対象から省く
        _targetIndex.Clear();
        foreach (var enemy in _enemy)
        {
            if (!enemy.IsStun)
            {
                _targetIndex.Add(enemy.PlayerIndex);
            }
        }

        // TODO: 後でAIを考える
        //_ThinkStatus.Value = ThinkStatus.kThink;
    }

    // TOOD: stateMachineは後で作成
    /*        public void Think() {
                _state.Execute();
            }*/

    /// <summary>
    /// 行動決定
    /// </summary>
    /// <returns></returns>
    public ActionInfo Think()
    {
        // TODO: 一旦ランダムで行動
        int target = _targetIndex
            .OrderBy(x => Guid.NewGuid()).FirstOrDefault();

        // debug
        /*
        if (MyIndex == 3)
        {
            target = 2;
        }
        if (MyIndex == 2)
        {
            target = 3;
        }
        */

        SkillModel skill = _my.PlayerModel.GetAvailableSkills()
            .OrderBy(x => Guid.NewGuid()).FirstOrDefault();

        return new ActionInfo(MyIndex, target, skill);
    }
}
