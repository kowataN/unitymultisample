using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public sealed class ActionManager
{
    private int _createCount = 0;

    /// <summary>
    /// 行動順
    /// </summary>
    private List<int> _actionOrder = new List<int>();
    public List<int> ActionOrder => _actionOrder;
    public List<ActionInfo> OrderList { get; set; } = new List<ActionInfo>();

    // よく使うので保持しておく
    private string[] _playerNames = default;


    /// <summary>
    /// プレイヤー毎の行動情報
    /// </summary>
    private ActionInfo[] _baseActionInfos;
    public ActionInfo[] BaseActionInfos => _baseActionInfos;

    public ActionManager(int count)
    {
        _createCount = count;
        _baseActionInfos = new ActionInfo[_createCount];
        for (int i = 0; i < _createCount; i++)
        {
            _baseActionInfos[i] = new ActionInfo(i, -1, new SkillModel());
        }
    }

    public void Init(BattlePresenter battlePresenter)
    {
        // プレイヤー名を取得しておく
        _playerNames = new string[GameTypes.MaxBattlePlayerNumber];
        for (int i = 0; i < _playerNames.Length; ++i)
        {
            var model = battlePresenter.GetPlayerModel(i);
            _playerNames[i] = model == default ? (i + 1).ToString() + "P" : model.PlayerModel.PlayerName;
        }
    }

    public void SetActionInfo(ActionInfo action) => _baseActionInfos[action.MyIndex] = action;
    public ActionInfo GetActionInfo(int myIndex) => _baseActionInfos[myIndex];

    /// <summary>
    /// 行動順を設定します。
    /// </summary>
    public void SetActionOrder()
    {
        // 守&盾→スキルの速度順

        IEnumerable<ActionInfo> srcActInfo = _baseActionInfos.Where(x => x.TargetIndex != -1);

        if (Debug.isDebugBuild)
        {
            string[] name = { "1P", "2P", "3P", "4P" };
            foreach (ActionInfo info in srcActInfo)
            {
                //DebugLogger.Log($"sort前 idx:{name[info.PlayerIndex]}  tgt:{name[info.TargetPlayerIndex]}  Act:{info.Skill?.Name}");
            }
        }

        // まず防御系行動を取得
        IEnumerable<ActionInfo> actGuard = srcActInfo.Where(x =>
           x.Skill.Type == SkillTypes.Guard || x.Skill.Type == SkillTypes.Shield);

        // 次に攻撃系行動を取得し速度で降順
        IEnumerable<ActionInfo> actAttack = srcActInfo.Where(x =>
           x.Skill.Type == SkillTypes.Attack || x.Skill.Type == SkillTypes.Magic)
            .OrderByDescending(x => x.Skill.Speed);

        // actGuard + actAtkSpdの順に結合
        OrderList.Clear();
        if (actGuard.Count() > 0)
        {
            OrderList.AddRange(actGuard);
        }
        if (actAttack.Count() > 0)
        {
            OrderList.AddRange(actAttack);
        }

        if (Debug.isDebugBuild)
        {
            string[] name = { "1P", "2P", "3P", "4P" };
            foreach (ActionInfo info in OrderList)
            {
                //DebugLogger.Log($"sort後 idx:{name[info.PlayerIndex]}  tgt:{name[info.TargetPlayerIndex]}  Act:{info.Skill?.Name}");
            }
        }
    }
}
