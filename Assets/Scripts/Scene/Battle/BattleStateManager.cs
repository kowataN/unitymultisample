﻿using State;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// バトルの状態遷移
/// </summary>
public sealed class BattleStateManager : MonoBehaviour
{
    private BattlePresenter _presenter = default;
    private StateBase _state;

    private void Awake()
    {
        _presenter = GetComponentInChildren<BattlePresenter>();

        // 起動初期化を設定
        _state = new StateInit(ExecuteStateInit, _presenter);
    }

    private void Update()
    {
        Execute();
    }

    /// <summary>
    /// ステートマシンを実行します
    /// </summary>
    public void Execute()
    {
        if (_presenter.IsStateStop)
        {
            return;
        }

        // 登録されているステートメソッドを実行する
        if (_state != null)
        {
            DebugBattleView.Inst.SetMainStateText(_state.GetStateName());
            _state.Execute();
        }
    }

    /// <summary>
    /// 起動初期化を行います
    /// </summary>
    private void ExecuteStateInit()
    {
        if (_state.StateEnd == true)
        {
            _state = new StateBattleRefresh(CheckStateBattleRefresh, _presenter);
        }
    }

    /// <summary>
    /// リフレッシュ状態のチェックを行います
    /// </summary> 
    private void CheckStateBattleRefresh()
    {
        if (_state.StateEnd == true)
        {
            _state = new StateInputWaiting(CheckStateInputWaiting, _presenter);
        }
    }

    /// <summary>
    /// 入力受付状態のチェックを行います
    /// </summary>
    private void CheckStateInputWaiting()
    {
        if (_state.StateEnd == true)
        {
            _state = new StateInputCheck(CheckStateInputCheck, _presenter);
        }
    }

    /// <summary>
    /// 入力情報妥当性判定状態のチェックを行います
    /// </summary>
    private void CheckStateInputCheck()
    {
        if (_state.StateEnd == true)
        {
            _state = new StateJudgeActOrder(CheckStateJudgeActOrder, _presenter);
        }
    }

    /// <summary>
    /// 行動順番判定状態のチェックを行います
    /// </summary>
    private void CheckStateJudgeActOrder()
    {
        if (_state.StateEnd == true)
        {
            _state = new StateBattleLoop(CheckStateBattleLoop, _presenter);
        }
    }

    /// <summary>
    /// バトルループ状態のチェックを行います
    /// </summary>
    private void CheckStateBattleLoop()
    {
        if (_state.StateEnd == true)
        {
            _state = new StateUpdateStatus(CheckStateJudgeEnd);
        }
    }

    /// <summary>
    /// 終了判定を行います
    /// </summary>
    private void CheckStateJudgeEnd()
    {
        if (_state.StateEnd == true)
        {
            var isEnd = _presenter.IsEndBattle;
            if (isEnd)
            {
                _state = new StateBattleEnd(CheckStateBattleEnd, _presenter);
            }
            else
            {
                _state = new StateUpdateTurn(CheckStateUpdateStatus, _presenter);
            }
        }
    }
    /// <summary>
    /// ステータス更新状態のチェックを行います
    /// </summary>
    private void CheckStateUpdateStatus()
    {
        if (_state.StateEnd == true)
        {
            // リフレッシュからやり直し
            _state = new StateBattleRefresh(CheckStateBattleRefresh, _presenter);
        }
    }

    private void CheckStateBattleEnd()
    {
        if (_state.StateEnd == true)
        {
            _state = null;

            // 結果を保存する
            _presenter.SaveBattleResult();

            Fader.Inst.BlackOut(1.0f, () => {
                // シーン遷移
                SceneManager.LoadScene("BattleResult");
            });
        }
    }
}
