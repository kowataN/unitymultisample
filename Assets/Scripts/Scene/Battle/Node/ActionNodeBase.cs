using System.Collections;

public enum ActionNodeType
{
    Message = 0,
    Guard,
    Shield,
    SkillInvoke,
    Reflection,
    BreakShield,
    GetHit,
    AddPoint,
    AddMP,
    AddHP,
    Stun,
}

public class ActionNodeBase
{
    protected int _index = 0;

    protected ActionNodeType _nodeType = ActionNodeType.Message;
    public ActionNodeType NodeType => _nodeType;

    public virtual IEnumerator Invoke(BattlePresenter presenter) 
    {
        yield break;
    }
}
