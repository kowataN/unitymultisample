using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class AddHPActionNode : ActionNodeBase
{
    private int _point = 0;

    public AddHPActionNode(int index, int point)
    {
        _nodeType = ActionNodeType.AddHP;
        _index = index;
        _point = point;
    }

    public override IEnumerator Invoke(BattlePresenter presenter)
    {
        presenter.ShowDmageValue(_index, _point);
        yield return PlayAddValueAnime.PlayAddHP(presenter.GetStatusPresenter(_index), _point);
    }
}
