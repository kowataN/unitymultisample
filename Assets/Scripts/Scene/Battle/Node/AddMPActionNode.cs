using System.Collections;

public sealed class AddMPActionNode : ActionNodeBase
{
    private int _point = 0;

    public AddMPActionNode(int index, int point)
    {
        _nodeType = ActionNodeType.AddMP;
        _index = index;
        _point = point;
    }

    public override IEnumerator Invoke(BattlePresenter presenter)
    {
        yield return PlayAddValueAnime.PlayAddMP(presenter.GetStatusPresenter(_index), _point);
    }
}
