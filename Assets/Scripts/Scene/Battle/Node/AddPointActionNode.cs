using System.Collections;

public sealed class AddPointActionNode : ActionNodeBase
{
    private int _point = 0;

    public AddPointActionNode(int index, int point)
    {
        _nodeType = ActionNodeType.AddPoint;
        _index = index;
        _point = point;
    }

    public override IEnumerator Invoke(BattlePresenter presenter)
    {
        yield return PlayAddValueAnime.PlayAddPoint(presenter.GetStatusPresenter(_index), _point);
    }
}
