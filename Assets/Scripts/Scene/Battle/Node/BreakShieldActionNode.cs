using System.Collections;

public sealed class BreakShieldActionNode : ActionNodeBase
{
    public BreakShieldActionNode(int index)
    {
        _nodeType = ActionNodeType.BreakShield;
        _index = index;
    }

    public override IEnumerator Invoke(BattlePresenter presenter)
    {
        presenter.SetDisplayShield(_index, false);
        yield return null;
    }
}
