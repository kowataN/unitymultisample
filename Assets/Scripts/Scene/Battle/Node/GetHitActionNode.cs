using System.Collections;
using UnityEngine;

public sealed class GetHitActionNode : ActionNodeBase
{
    public GetHitActionNode(int index)
    {
        _nodeType = ActionNodeType.GetHit;
        _index = index;
    }

    public override IEnumerator Invoke(BattlePresenter presenter)
    {
        bool isEnd = false;
        var model = presenter.GetPlayerModel(_index);
        model.PlayUnitAnimation(UnitMotionType.GetHit,
            () => {
                if (model.IsStun)
                {
                    model.PlayUnitAnimation(UnitMotionType.Stun);
                }
                else
                {
                    model.PlayUnitAnimation(UnitMotionType.Idle);
                }
                isEnd = true;
            }
        );
        if (isEnd)
        {
            yield break;
        }
        yield return null;
    }
}
