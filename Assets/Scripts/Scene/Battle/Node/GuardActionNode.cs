using System.Collections;
using UnityEngine;

public sealed class GuardActionNode : ActionNodeBase
{
    public GuardActionNode(int index)
    {
        _nodeType = ActionNodeType.Guard;
        _index = index;
    }

    public override IEnumerator Invoke(BattlePresenter presenter)
    {
        bool IsEnd = false;
        var model = presenter.GetPlayerModel(_index);
        model.PlayUnitAnimation(UnitMotionType.Defense,
            () => {
                model.PlayUnitAnimation(UnitMotionType.Idle);
                IsEnd = true;
            }
        );
        yield return new WaitUntil(() => IsEnd == true);
    }
}
