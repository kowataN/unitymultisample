using System.Collections;
using UnityEngine;

public sealed class InvokeSkillActionNode : ActionNodeBase
{
    public InvokeSkillActionNode(int index)
    {
        _nodeType = ActionNodeType.SkillInvoke;
        _index = index;
    }

    public override IEnumerator Invoke(BattlePresenter presenter)
    {
        bool isEnd = false;
        var model = presenter.GetPlayerModel(_index);
        model.PlayUnitAnimation(UnitMotionType.Attack,
            () => {
                model.PlayUnitAnimation(UnitMotionType.Idle);
                if (_index == 0)
                {
                    presenter.BattleView.UnitSelectMarkerView.SetPosition(_index);
                }
                isEnd = true;
            }
        );
        yield return new WaitUntil(() => isEnd == true);
    }
}
