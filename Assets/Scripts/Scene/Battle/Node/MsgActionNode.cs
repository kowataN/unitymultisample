using System;
using System.Collections;

public sealed class MsgActionNode : ActionNodeBase
{
    private string _msg = default;

    public MsgActionNode(string msg)
    {
        _nodeType = ActionNodeType.Message;
        _msg = msg;
    }

    public override IEnumerator Invoke(BattlePresenter presenter)
    {
        DebugLogger.Log(_msg);
        yield return presenter.BattleView.MessageView.TypeMessage(
            _msg + Environment.NewLine);
    }
}
