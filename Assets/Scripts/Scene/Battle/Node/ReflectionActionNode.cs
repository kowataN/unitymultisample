using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class ReflectionActionNode : ActionNodeBase
{
    public ReflectionActionNode(int index)
    {
        _nodeType = ActionNodeType.Reflection;
        _index = index;
    }

    public override IEnumerator Invoke(BattlePresenter presenter)
    {
        //DebugLogger.Log("ReflectionActionNode.Invoke　開始");

        //DebugLogger.Log("ReflectionActionNode.Invoke　終了");
        yield return null;
    }
}
