using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class RotateUnitActionNode : ActionNodeBase
{
    private int _target = -1;

    public RotateUnitActionNode(int index, int target)
    {
        _nodeType = ActionNodeType.SkillInvoke;
        _index = index;
        _target = target;
    }

    public override IEnumerator Invoke(BattlePresenter presenter)
    {
        DebugLogger.Log("RotateUnitActionNode invoke");
        yield return presenter.UpdateTargetRotate(_index, _target);
    }
}
