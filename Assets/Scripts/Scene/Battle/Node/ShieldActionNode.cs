using System.Collections;
using UnityEngine;

public sealed class ShieldActionNode : ActionNodeBase
{
    public ShieldActionNode(int index)
    {
        _nodeType = ActionNodeType.Shield;
        _index = index;
    }

    public override IEnumerator Invoke(BattlePresenter presenter)
    {
        bool isEnd = false;
        var model = presenter.GetPlayerModel(_index);
        model.PlayUnitAnimation(UnitMotionType.Defense,
            () => {
                model.PlayUnitAnimation(UnitMotionType.Idle);
                presenter.SetDisplayShield(_index, true);
                isEnd = true;
            }
        );
        yield return new WaitUntil(() => isEnd == true);
    }
}
