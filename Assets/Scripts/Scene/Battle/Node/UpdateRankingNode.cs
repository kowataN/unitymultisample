using System.Collections;

public class UpdateRankingNode : ActionNodeBase
{
    public UpdateRankingNode()
    {
    }

    public override IEnumerator Invoke(BattlePresenter presenter)
    {
        presenter.UpdateRanking();
        yield return null;
    }
}
