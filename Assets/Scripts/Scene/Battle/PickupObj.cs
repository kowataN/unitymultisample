﻿using UnityEngine;

public sealed class PickupObj : MonoBehaviour
{
    public System.Action<string, int> OnClickAction;
    private int _index = -1;
    public int Index => _index;
    private string _tag = default;

    public void SetIndex(int index) => _index = index;

    public void OnClickObj() => OnClickAction?.Invoke(this.tag, Index);
}
