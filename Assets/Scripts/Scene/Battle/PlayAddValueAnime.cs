using System.Collections;

public static class PlayAddValueAnime
{
    public static IEnumerator PlayAddPoint(StatusPresenter pre, int addValue)
    {
        yield return pre.AddPointAnime(addValue, 1f);
    }

    public static IEnumerator PlayAddHP(StatusPresenter pre, int addValue)
    {
        yield return pre.AddHPAnime(addValue, 1f);
    }

    public static IEnumerator PlayAddMP(StatusPresenter pre, int addValue)
    {
        yield return pre.AddMPAnime(addValue, 1f);
    }
}
