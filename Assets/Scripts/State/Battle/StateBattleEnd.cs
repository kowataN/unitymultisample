using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using State;

public class StateBattleEnd : StateBase
{
    private SubState _subState = default;
    private BattlePresenter _presenter = default;

    public StateBattleEnd(executeState execute, BattlePresenter presenter) : base(execute)
    {
        _presenter = presenter;
        _subState = new SubState(this.ExecutePlay);
    }

    public override void Execute()
    {
        DebugBattleView.Inst.SetSubStateText(_subState?.GetStateName());
        _subState?.Execute();
        ExecDelegate?.Invoke();
    }

    private void ExecutePlay()
    {
        _presenter.PlayBattleEnd(() => {
            ExecuteAnimeIdle();
        });

        _subState.StateEnd = false;
        _subState.ExecDelegate = null;
    }

    private void ExecuteAnimeIdle()
    {
        _subState.ExecDelegate = null;
        _subState = null;
        StateEnd = true;
    }
}
