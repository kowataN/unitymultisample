﻿/*
 * バトルループ
 *   行動順にアニメーション再生
 */

using DG.Tweening;
using State;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public sealed class StateBattleLoop : StateBase
{
    private SubState _subState = default;
    private BattlePresenter _battlePresenter = default;

    private List<ActionInfo> _actionInfo = default;
    private bool[] _endAction = default;
    private int _crtActionIndex = 0;
    private Dictionary<int, string> _playerNames = new Dictionary<int, string>();

    public StateBattleLoop(executeState execute, BattlePresenter presenter) : base(execute)
    {
        _battlePresenter = presenter;
        StateEnd = false;
        _subState = new SubState(this.ExecuteInit);
    }

    public override void Execute()
    {
        DebugBattleView.Inst.SetSubStateText(_subState?.GetStateName());
        _subState?.Execute();
        ExecDelegate?.Invoke();
    }

    private void ExecuteInit()
    {
        // 行動順取得
        _actionInfo = new List<ActionInfo>(_battlePresenter.ActionInfo);

        _endAction = new bool[_actionInfo.Count];
        _endAction.ToList().ForEach(x => x = false);

        _crtActionIndex = 0;

        // プレイヤー名を取得しておく
        for (int i = 0; i < GameTypes.MaxBattlePlayerNumber; ++i)
        {
            var playerModel = _battlePresenter.GetPlayerModel(i);
            _playerNames.Add(playerModel.PlayerIndex,
                playerModel == null ? (i + 1).ToString() + "P" : playerModel.PlayerModel.PlayerName);
        }

        // UI切り替え
        _battlePresenter.ChangeDisplayUI(BattleView.DisplayMode.Message);

        StateEnd = false;
        _subState.ExecDelegate = ExecuteBattleMain;
    }

    /// <summary>
    /// バトル開始
    ///  行動によって下記に分岐
    ///     ExecuteBattleGuardAndShieldAnimeStart
    ///     ExecuteBattleAttackAndMagicAnimeStart
    /// </summary>
    private void ExecuteBattleMain()
    {
        var actInfo = _actionInfo[_crtActionIndex];
        if (actInfo.TargetIndex == -1)
        {
            // 攻撃対象がいないので終了
            _subState.StateEnd = true;
            _subState.ExecDelegate = ExecuteBattleAnimeIdle;
        }
        else
        {
            // スキルのタイプで分岐
            switch (actInfo.Skill.Type)
            {
                // 防御系
                case SkillTypes.Guard:
                case SkillTypes.Shield:
                    _subState.ExecDelegate = ExecuteBattleGuardAndShieldAnimeStart;
                    break;
                // 攻撃系
                case SkillTypes.Attack:
                case SkillTypes.Magic:
                    _subState.ExecDelegate = ExecuteBattleAttackAndMagicAnimeStart;
                    break;
            }
            _subState.StateEnd = false;
        }
    }

    /// <summary>
    /// ガード、シールドアニメ再生
    ///     終了後、ExecuteBattleEndJudge
    /// </summary>
    private void ExecuteBattleGuardAndShieldAnimeStart()
    {
        ActionInfo myInfo = _actionInfo[_crtActionIndex];
        var actMsgList = ActionMsgCreator.CreateActionMsgGuardAndShield(
            _playerNames[myInfo.MyIndex], in myInfo);
        PlayActionMessageList(actMsgList, () => _subState.StateEnd = true);

        _subState.StateEnd = false;
        _subState.ExecDelegate = ExecuteBattleAnimeIdle;
    }

    /// <summary>
    /// 攻撃、魔法アニメ再生
    ///     相手の行動によって相手側にダメージアニメ再生
    ///     終了後、ExecuteBattleEndJudge
    /// </summary>
    private void ExecuteBattleAttackAndMagicAnimeStart()
    {
        // 相手の行動を確認
        ActionInfo myInfo = _actionInfo[_crtActionIndex];
        var tar = _actionInfo.Where(x => x.MyIndex == myInfo.TargetIndex);
        if (!tar.Any())
        {
            _subState.StateEnd = false;
            _subState.ExecDelegate = ExecuteBattleEndJudge;
            return;
        }
        ActionInfo targetInfo = tar.First();

        List<ActionNodeBase> actMsgList;

        // どちらかが気絶していたら終了
        if (_battlePresenter.IsStun(myInfo.MyIndex))
        {
            actMsgList = ActionMsgCreator.CreateActionMsgCanNotAttackMyStun(_playerNames[myInfo.MyIndex]);
            PlayActionMessageList(actMsgList, () => _subState.StateEnd = true);

            _subState.StateEnd = false;
            _subState.ExecDelegate = ExecuteBattleAnimeIdle;
            return;
        }
        else if (_battlePresenter.IsStun(myInfo.TargetIndex))
        {
            actMsgList = ActionMsgCreator.CreateActionMsgCanNotAttackTargetStun(
                _playerNames[myInfo.MyIndex],
                _playerNames[myInfo.TargetIndex]);
            PlayActionMessageList(actMsgList, () => _subState.StateEnd = true);

            _subState.StateEnd = false;
            _subState.ExecDelegate = ExecuteBattleAnimeIdle;
            return;
        }

        // 行動リスト作成
        var floatingInfo = new FloatingInfo() {
            Myself = new FloatingInfo.Base(myInfo.MyIndex) {
                Name = _playerNames[myInfo.MyIndex],
                CrtHP = _battlePresenter.GetPlayerCrtHP(myInfo.MyIndex),
                CrtPoint = _battlePresenter.GetPlayerCrtPoint(myInfo.MyIndex),
            },
            Target = new FloatingInfo.Base(myInfo.TargetIndex) {
                Name = _playerNames[myInfo.TargetIndex],
                CrtHP = _battlePresenter.GetPlayerCrtHP(myInfo.TargetIndex),
                CrtPoint = _battlePresenter.GetPlayerCrtPoint(myInfo.TargetIndex)
            },
            UsedMP = myInfo.Skill.Cost
        };

        actMsgList = ActionMsgCreator.CreateActionMsg(floatingInfo, in myInfo.Skill, in targetInfo.Skill);
        PlayActionMessageList(actMsgList, () => _subState.StateEnd = true);

        _subState.StateEnd = false;
        _subState.ExecDelegate = ExecuteBattleAnimeIdle;
    }

    private void ExecuteBattleAnimeIdle()
    {
        // メッセージ表示待ち
        if (_subState.StateEnd)
        {
            _subState.StateEnd = false;
            _subState.ExecDelegate = ExecuteBattleEndJudge;
        }
    }

    /// <summary>
    /// バトル終了判定
    ///     全てのプレイヤーの行動が完了したらステートを終わらせる。
    ///     そうでなければ、ExecuteBattleMainに戻す
    /// </summary>
    private void ExecuteBattleEndJudge()
    {
        _endAction[_crtActionIndex] = true;

        bool isEnd = _endAction.Where(x => x == true).Count() == _endAction.Length;
        if (isEnd)
        {
            StateEnd = true;
            _subState = new SubState(null);
        }
        else
        {
            _crtActionIndex++;
            _subState.StateEnd = false;
            // 再度バトルメインからやり直し
            _subState.ExecDelegate = ExecuteBattleMain;
        }
    }

    /// <summary>
    /// 行動リストを再生します。
    /// </summary>
    /// <param name="actionList"></param>
    private void PlayActionMessageList(List<ActionNodeBase> actMsgList, Action callback)
    {
        DebugBattleView.Inst.SetActionLog(in actMsgList);

        _battlePresenter.BattleView.MessageView.ResetMessage();
        _battlePresenter.StartCoroutine(PlayActionMessage(actMsgList, ()=> { 
            callback?.Invoke(); 
        }));
    }

    private IEnumerator PlayActionMessage(List<ActionNodeBase> actMsgList, Action callback)
    {
        foreach (var act in actMsgList)
        {
            yield return act.Invoke(_battlePresenter);
        }
        callback?.Invoke();
    }
}
