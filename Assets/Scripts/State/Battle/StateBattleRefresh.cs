﻿/*
 * リフレッシュ
 *   MP回復、状態異常復帰、開始アニメ再生
 * ExecuteInit
 * ExecuteRecoveryMpIdle
 * ExecuteRecoveryAbnormalStart
 * ExecuteStartAnimationInit
 * ExecuteStartAnimationIdle
 *
 */
using State;

public sealed class StateBattleRefresh : StateBase
{
    private SubState _subState;
    BattlePresenter _battlePresenter;

    public StateBattleRefresh(executeState execute, BattlePresenter presenter) : base(execute)
    {
        _battlePresenter = presenter;

        StateEnd = false;
        _subState = new SubState(this.ExecuteInit);
    }

    public override void Execute()
    {
        DebugBattleView.Inst.SetSubStateText(_subState?.GetStateName());
        _subState?.Execute();
        ExecDelegate?.Invoke();
    }

    private void ExecuteInit()
    {
        for (int i = 0; i < GameTypes.MaxBattlePlayerNumber; ++i)
        {
            _battlePresenter.SetDisplayShield(i, false);
        }

        // 一旦ユニット選択マーカーを消す
        _battlePresenter.BattleView.UnitSelectMarkerView.SetPosition(0);

        _subState.StateEnd = false;
        _subState.ExecDelegate = ExecuteRecoveryMpIdle;
    }

    /// <summary>
    /// MP回復
    /// </summary>
    private void ExecuteRecoveryMpIdle()
    {
        // 全プレイヤーMP回復
        _battlePresenter.RecoveryMPAllPlayer();
        _battlePresenter.SetBattleMessage($"MPが{_battlePresenter.RecoveryMPValue.ToString()}回復しました。", true);

        _subState.StateEnd = false;
        _subState.ExecDelegate = ExecuteRecoveryAbnormal;
    }

    /// <summary>
    /// 状態異常復帰
    /// </summary>
    private void ExecuteRecoveryAbnormal()
    {
        for (int i=0; i< GameTypes.MaxBattlePlayerNumber; i++)
        {
            var model = _battlePresenter.GetPlayerModel(i);
            if (model.IsStun)
            {
                // 前のターンに気絶していたので復活させる
                if (model.Abnormal.Equals(AbnormalType.Stun))
                {
                    model.SetAbnormalStatus(AbnormalType.Normal);
                    model.AddHitPoint(model.UnitModel.UnitStatus.Status.HP);
                    model.PlayUnitAnimation(UnitMotionType.Idle);
                }
                else
                {
                    model.SetAbnormalStatus(AbnormalType.Stun);
                    model.PlayUnitAnimation(UnitMotionType.Stunning);
                }
            }
        }

        _subState.StateEnd = false;
        _subState.ExecDelegate = ExecuteStartAnimationInit;
    }

    /// <summary>
    /// ターン開始アニメ
    /// </summary>
    private void ExecuteStartAnimationInit()
    {
        _subState.StateEnd = false;
        _subState.ExecDelegate = null;

        // ターン開始アニメ再生
        _battlePresenter.TurnStartAnime.Play(_battlePresenter.CrtTurn);
        _battlePresenter.TurnStartAnime.EndAnimeCallback = () => {
            ExecuteStartAnimationIdle();
        };
    }

    private void ExecuteStartAnimationIdle()
    {
        _subState.ExecDelegate = null;
        _subState = null;
        StateEnd = true;
    }
}
