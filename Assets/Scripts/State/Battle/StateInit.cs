﻿using State;

/// <summary>
/// 起動初期化
/// </summary>
public class StateInit : StateBase
{
    private SubState _subState = default;
    private BattlePresenter _presenter = default;

    public StateInit(executeState execute, BattlePresenter presenter) : base(execute)
    {
        _presenter = presenter;
        StateEnd = false;
        _subState = new SubState(ExecuteInit);
        _subState.StateEnd = false;
    }

    public override void Execute()
    {
        DebugBattleView.Inst.SetSubStateText(_subState.GetStateName());
        _subState?.Execute();
        ExecDelegate?.Invoke();
    }

    private void ExecuteInit()
    {
        _presenter.SetUp();

        // 全キャラがCPUの場合はプレイヤー0を使う
        if (_presenter.MyIndex == -1)
        {
            _presenter.MyIndex = 0;
        }

        _subState.ExecDelegate = ExecuteFadeInInit;
    }

    private void ExecuteFadeInInit()
    {
        _subState.ExecDelegate = ExecuteFadeInLoop;
    }

    private void ExecuteFadeInLoop()
    {
        StateEnd = true;
    }
}
