﻿/*
 * 入力値チェック
 *   入力人数確認、各同期も行う
 */
using State;

public sealed class StateInputCheck : StateBase
{
    private BattlePresenter _presenter = default;
    public StateInputCheck(executeState execute, BattlePresenter presenter) : base(execute)
    {
        _presenter = presenter;
    }

    public override void Execute()
    {
        // TODO: 入力値のチェックを行う
        bool isCheck = _presenter.CheckAction();
        //DebugLogger.Log($"入力チェック[{isCheck}]");

        StateEnd = true;
        ExecDelegate?.Invoke();
    }
}
