﻿/*
 * 入力受付
 *   状態異常チェックも行う
 */
using State;
using UnityEngine;

public sealed class StateInputWaiting : StateBase {
    private SubState _subState;
    private float deltaTime = 0.0f;
    BattlePresenter _battlePresenter = default;

    public StateInputWaiting(executeState execute, BattlePresenter battlePresenter) : base(execute) {
        _subState = new SubState(this.ExecuteInit);
        _battlePresenter = battlePresenter;
    }
    public override void Execute() {
        DebugBattleView.Inst.SetSubStateText(_subState?.GetStateName());
        _subState?.Execute();
        ExecDelegate?.Invoke();
    }

    private void ExecuteInit() {
        StateEnd = false;
        _subState.StateEnd = false;
        _subState.ExecDelegate = ExecuteIdle;

        // UI切り替え
        _battlePresenter.ChangeDisplayUI(BattleView.DisplayMode.Skill);

        _battlePresenter.InitAtTurnStart();
        _battlePresenter.IsTouchUnitEnabled = true;
    }

    private void ExecuteIdle() {
        /*
         * 何らかの処理
         * 終了判定後に、_subState.StateEndをtrueにする
         */

        // TODO: 一旦ここで行動決定
        deltaTime += Time.deltaTime;
        if (deltaTime >= 1.0f) {
            _battlePresenter.Think();
            deltaTime = 0.0f;
        }

        // タイマー更新をココらへんに
        _battlePresenter.UpdateTimer();
        _subState.StateEnd = _battlePresenter.NowTimer <= 0.0f;

        //_subState.StateEnd = false;

        // 相手の方を向く
        _battlePresenter.UpdateUnitRotate();

        if (_subState.StateEnd == true)
        {
            _battlePresenter.IsTouchUnitEnabled = false;

            _subState.StateEnd = false;
            _subState.ExecDelegate = null;
            StateEnd = true;
        }
    }
}
