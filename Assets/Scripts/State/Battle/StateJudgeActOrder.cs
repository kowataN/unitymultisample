﻿/*
 * 行動順番判定
 *   入力スキルに応じて行動順判定
 */
using State;

public class StateJudgeActOrder : StateBase
{
    private BattlePresenter _presenter = default;

    public StateJudgeActOrder(executeState execute, BattlePresenter presenter) : base(execute)
    {
        _presenter = presenter;
    }

    public override void Execute()
    {
        // 行動順決定
        _presenter.SetActionOrder();

        StateEnd = true;
        ExecDelegate?.Invoke();
    }
}

