﻿/*
 * 各ステータス更新
 *   状態異常更新
 */
using State;

public class StateUpdateStatus : StateBase
{
    private SubState _subState = default;

    public StateUpdateStatus(executeState execute) : base(execute) { }

    public override void Execute()
    {
        DebugBattleView.Inst.SetSubStateText(_subState?.GetStateName());
        StateEnd = true;
        _subState?.Execute();
        ExecDelegate?.Invoke();
    }
}
