﻿/*
 * ターン更新
 */
using State;

public class StateUpdateTurn : StateBase
{
    private BattlePresenter _presenter = default;

    public StateUpdateTurn(executeState execute, BattlePresenter presenter) : base(execute)
    {
        _presenter = presenter;
    }

    public override void Execute()
    {
        _presenter.AddTurn();

        StateEnd = true;
        ExecDelegate?.Invoke();
    }
}

