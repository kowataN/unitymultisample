using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BattleEndAnimeView : MonoBehaviour
{
    [SerializeField] private Animator _animator = default;
    [SerializeField] private string _timeLineName = default;

    public UnityAction EndAnimeCallback = default;

    public void Play()
    {
        _animator.Play(_timeLineName);
    }

    public void EndCallback()
    {
        EndAnimeCallback?.Invoke();
    }
}
