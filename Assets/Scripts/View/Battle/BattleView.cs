﻿using System;
using System.Collections.Generic;
using UnityEngine;

public sealed class BattleView : MonoBehaviour
{
    [SerializeField] private GameObject[] _playerNodes = default;
    [SerializeField] private GameObject[] _unitNodes = default;
    [SerializeField] private GameObject[] _unitCamera = default;
    [SerializeField] private GameObject[] _shieldNodes = default;
    [SerializeField] private GameObject _skillList = default;
    [SerializeField] private GameObject _selectMarker = default;
    [SerializeField] private StatusView[] _unitsStatus = default;
    [SerializeField] private TurnTimerView _turnTimerView = default;
    // スキル情報
    [SerializeField] private SkillPanelView _skillPanelView = default;
    // スキルボタンを取得する
    [SerializeField] private PartsSkillIconView[] _skillIconList = default;
    // メッセージビュー
    [SerializeField] private MessageView _messageView = default;
    // ターン
    [SerializeField] private TurnView _turnView = default;
    // ユニット選択マーカー
    [SerializeField] private UnitSelectMarkerView _unitSelectMarkerView = default;
    /// <summary>
    /// ターン開始アニメ
    /// </summary>
    [SerializeField] private TurnStartAnimeView _turnStartAnimeView = default;
    /// <summary>
    /// ゲーム終了アニメ
    /// </summary>
    [SerializeField] private BattleEndAnimeView _gameEndAnimeView = default;

    public GameObject[] PlayerNodes => _playerNodes;
    public GameObject[] UnitNodes => _unitNodes;
    public GameObject[] ShieldNodes => _shieldNodes;
    public GameObject SkillList => _skillList;
    public StatusView[] UnitStatusView => _unitsStatus;
    public TurnTimerView TurnTimerView => _turnTimerView;
    public SkillPanelView SkillPanelView => _skillPanelView;
    public PartsSkillIconView[] SkillIconList => _skillIconList;
    public GameObject SkillSelectMarker => _selectMarker;
    public MessageView MessageView => _messageView;
    public TurnView TurnView => _turnView;
    public UnitSelectMarkerView UnitSelectMarkerView => _unitSelectMarkerView;
    public TurnStartAnimeView TurnStartAnimeView => _turnStartAnimeView;
    public BattleEndAnimeView GameEndAnimeView => _gameEndAnimeView;

    public void SetUnitCamera(int myIndex)
    {
        for (int i = 0; i < _unitCamera.Length; ++i)
        {
            // 自分用のカメラのみ有効にする
            _unitCamera[i].SetActive(i == myIndex);
        }
    }

    public void CreateSkillIconList(List<SkillModel> list)
    {
        // スキルアイコン表示
        int dispCount = Math.Min(6, list.Count);
        for (int i = 0; i < dispCount; i++)
        {
            GameObject skillIcon = PrefabCreator.CreateSkillIcon(SkillList.transform);
            var skillScript = skillIcon.GetComponent<PartsSkillIconView>();
            if (skillScript == null)
            {
                continue;
            }

            SkillModel skill = list[i];
            skillScript.SetData(i, skill);
        }

        _skillIconList = SkillList.GetComponentsInChildren<PartsSkillIconView>();
    }

    public enum DisplayMode
    {
        Skill, Message
    }
    public void ChangeDisplayUI(DisplayMode mode)
    {
        _messageView.SetVisible(mode == DisplayMode.Message);
        _skillPanelView.SetVisible(mode == DisplayMode.Skill);
    }

    public void SetMainCameraPos(int index)
    {
        if (index < 0 && _unitCamera.Length > index)
        {
            return;
        }

        Camera camera = Camera.main;
        camera.transform.position = _unitCamera[index].transform.position;
        camera.transform.rotation = _unitCamera[index].transform.rotation;
    }

    //public void UpdateUnitRotate(int index, int targetIndex)
    //{
    //    if (index == targetIndex)
    //    {
    //        return;
    //    }

    //    float speed = 2f;
    //    Vector3 direction = PlayerNodes[targetIndex].transform.position - PlayerNodes[index].transform.position;
    //    Quaternion toRotation = Quaternion.FromToRotation(PlayerNodes[index].transform.forward, direction);

    //    toRotation = new Quaternion(0f, GameTypes.RotateTbl[index][targetIndex], 0f, 0f);

    //    Quaternion retRot = Quaternion.Lerp(PlayerNodes[index].transform.rotation, toRotation, speed * Time.deltaTime);

    //    retRot.x = retRot.z = 0f;

    //    PlayerNodes[index].transform.rotation = retRot;
    //}
}
