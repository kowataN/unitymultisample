using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugBattleView : SingletonMonoBhv<DebugBattleView>
{
    [SerializeField] private BattlePresenter _battlePresenter = default;
    [SerializeField] private Text _txtMainState = default;
    [SerializeField] private Text _txtSubState = default;
    [SerializeField] private Text _actionLog = default;
    [SerializeField] private Text _txtStateStop = default;
    [SerializeField] private Button[] _btnCameraList = default;
    [SerializeField] private Text _txtTimeScale = default;

    public void SetMainStateText(string msg)
    {
        if (Debug.isDebugBuild == false)
        {
            return;
        }
        if (_txtMainState == null)
        {
            return;
        }
        _txtMainState.text = "Main: " + msg;
    }

    public void SetSubStateText(string msg)
    {
        if (Debug.isDebugBuild == false)
        {
            return;
        }
        if (_txtSubState == null)
        {
            return;
        }
        _txtSubState.text = " Sub: " + msg;
    }

    public void OnClickCamera(int index) => _battlePresenter.SetMainCameraPos(index);

    public void SetActionLog(in List<ActionNodeBase> list)
    {
        if (_actionLog == null)
        {
            return;
        }

        ResetActionLog();
        list.ForEach(x => {
            _actionLog.text += $"type[{x.NodeType.ToString()}]" + Environment.NewLine;
            //_actionLog.text += $"msg[x.msg}]" + Environment.NewLine;
            _actionLog.text += "----------------------------------" + Environment.NewLine;
        });
    }

    public void ResetActionLog() => _actionLog.text = "";

    public void OnClickStop()
    {
        _battlePresenter.IsStateStop = !_battlePresenter.IsStateStop;
        _txtStateStop.text = (_battlePresenter.IsStateStop) ? "停止中" : "起動中";
    }

    public void OnClickTimeScale()
    {
        Time.timeScale += 1.0f;
        if (Time.timeScale >= 4.0f)
        {
            Time.timeScale = 1.0f;
        }

        _txtTimeScale.text = new string('▶', (int)Time.timeScale);
    } 
}
