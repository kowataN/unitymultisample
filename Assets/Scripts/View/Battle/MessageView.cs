using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public sealed class MessageView : MonoBehaviour
{
    [SerializeField] private Text _message = default;
    public enum TypingMode
    {
        Idle, Typing
    }
    private TypingMode _isTyping = TypingMode.Idle;
    public TypingMode IsTyping => _isTyping;

    public void ResetMessage() => _message.text = "";

    public void SetVisible(bool value) => gameObject.SetActive(value);

    public IEnumerator TypeMessage(string msg)
    {
        _isTyping = TypingMode.Typing;
        foreach (char c in msg)
        {
            _message.text += c;
            yield return new WaitForSeconds(1f / 20);
        }

        _isTyping = TypingMode.Idle;
    }
}
