﻿using UnityEngine;
using UnityEngine.UI;

public sealed class PartsSkillIconView : MonoBehaviour
{
    [SerializeField] private int _Index = default;
    [SerializeField] private Text _textRank = default;
    [SerializeField] private Text _textCost = default;
    [SerializeField] private Button _button = default;
    [SerializeField] private Image _icon = default;

    public int Index => _Index;

    public Button Button => _button;

    public void SetData(int index, SkillModel skill)
    {
        _Index = index;
        _textRank.text = skill.Rank.ToString();
        _textCost.text = skill.Cost.ToString();
        int skillIndex = (int)(skill.Type) - 1;
        _icon.sprite = ImageStorage.Inst.GetSkillIcon(skillIndex);
    }
}
