﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ParamValueView : MonoBehaviour
{
    [SerializeField] private Text _addValue = default;

    public void ShowAddAnime(int value, TweenCallback callback)
    {
        _addValue.text = Mathf.Abs(value).ToString();
        Color startCol = ParamValueTypes.StartColor;
        Color endCol = startCol;
        startCol.a = 1;

        Vector3 pos = _addValue.transform.localPosition;
        pos.y = 30.0f;
        _addValue.transform.localPosition = pos;

        gameObject.SetActive(true);

        var seq = DOTween.Sequence();
        // 徐々に表示する→その後下に移動しながら表示を消していく
        seq.Append(_addValue.DOColor(startCol, 0.3f).SetEase(Ease.Linear))
            .AppendInterval(0.7f)
            .Append(_addValue.transform.DOLocalMoveY(15.0f, 0.3f))
            .Join(_addValue.DOColor(endCol, 0.3f).SetEase(Ease.Linear))
            .OnComplete(() => {
                gameObject.SetActive(false);
                callback?.Invoke();
            });
    }
}
