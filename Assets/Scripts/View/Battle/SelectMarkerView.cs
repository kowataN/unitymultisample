﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public sealed class SelectMarkerView : MonoBehaviour
{
    [SerializeField] private Image _image = default;

    void Update()
    {
        _image.transform.Rotate(0.0f, 0.0f, 0.5f);
    }
}
