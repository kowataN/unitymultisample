﻿using UnityEngine;
using UnityEngine.UI;

public sealed class SkillPanelView : MonoBehaviour
{
    [SerializeField] private Image _skillIcon = default;
    [SerializeField] private Text _skillName = default;
    [SerializeField] private Text _cost = default;

    public void UpdateSkillDisply(SkillModel? skill)
    {
        int skillIndex = (int)(skill?.Type) - 1;
        _skillIcon.sprite = ImageStorage.Inst.GetSkillIcon(skillIndex);
        _skillName.text = skill?.Name;
        _cost.text = skill?.Cost.ToString();
    }

    public void SetVisible(bool value) => gameObject.SetActive(value);
}
