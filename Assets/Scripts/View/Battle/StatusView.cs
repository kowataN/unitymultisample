﻿using UnityEngine;
using UnityEngine.UI;

public sealed class StatusView : MonoBehaviour
{
    [SerializeField] private Text _nameText = default;
    [SerializeField] private Text _hp = default;
    [SerializeField] private Text _mp = default;
    [SerializeField] private Text _attribute = default;
    [SerializeField] private Text _ranking = default;
    [SerializeField] private Text _point = default;
    [SerializeField] private PlayerType _type = PlayerType.CPU;
    private int _rankingNumber = 0;

    public Text HP => _hp;
    public Text MP => _mp;
    public Text Point => _point;
    public int Ranking => _rankingNumber;


    public PlayerType PlayerType
    {
        get => _type;
        set => _type = value;
    }

    private void Start()
    {
        _ranking.text = "1st";
        _point.text = "200";
    }

    public void SetData(BattlePlayerModel player, int ranking)
    {
        _nameText.text = player.PlayerModel.PlayerName;
        SetRanking(ranking);
        _type = player.PlayerType;
        _attribute.text = Attribute.GetString(player.UnitAttribute);
    }

    public void SetRanking(int ranking)
    {
        _rankingNumber = ranking;
        _ranking.text =
            ranking == 1 ? "1st"
            : ranking == 2 ? "2nd"
            : ranking == 3 ? "3rd"
            : "4th";
    }
}
