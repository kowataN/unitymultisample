using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TurnStartAnimeView : MonoBehaviour
{
    [SerializeField] private GameObject _bg = default;
    [SerializeField] private Text _crtTurnText = default;
    [SerializeField] private Text _maxTurnText = default;
    [SerializeField] private Animator _animator = default;

    public UnityAction EndAnimeCallback = default;

    public void SetUp(in int crtTurn, in int maxTurn)
    {
        SetCrtTurn(crtTurn);
        _maxTurnText.text = maxTurn.ToString();
    }

    public void Play(in int crtTurn)
    {
        SetCrtTurn(crtTurn);
        _animator.Play("TurnStart");
    }

    private void SetCrtTurn(in int turn) => _crtTurnText.text = turn.ToString();

    public void EndCallback()
    {
        EndAnimeCallback?.Invoke();
    }
}
