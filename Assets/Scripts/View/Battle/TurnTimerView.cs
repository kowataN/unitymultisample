using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnTimerView : MonoBehaviour
{
    [SerializeField] private Slider _slider = default;

    public void SetMaxTimerValue(float value)
    {
        _slider.maxValue = value;
        _slider.value = value;
    }

    public void ResetValue() => _slider.value = _slider.maxValue;

    public void SetNowTimer(float value) => _slider.value = value;
}
