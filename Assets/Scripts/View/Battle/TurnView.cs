using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public sealed class TurnView : MonoBehaviour
{
    [SerializeField] private Text _txtCrtTurn = default;
    [SerializeField] private Text _txtMaxTurn = default;

    public Text CrtTurn => _txtCrtTurn;
    public Text MaxTurn => _txtMaxTurn;
}
