using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSelectMarkerView : MonoBehaviour
{
    [SerializeField] private GameObject _selectMarker = default;
    [SerializeField] private Vector2[] _positions = default;

    public void SetVisible(bool val) => _selectMarker.SetActive(val);

    public void SetPosition(int index)
    {
        _selectMarker.transform.localPosition = _positions[index];
    }
}
