using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Linq;

public class BattleResultView : MonoBehaviour
{
    [SerializeField] private Image _fadeImage = default;
    [SerializeField] private GameObject[] _Infos = default;
    private UnityAction _endShowRankingCallback = null;
    [SerializeField] private List<ResultPlayerView> _parts = default;

    private void Awake()
    {
        _fadeImage.gameObject.SetActive(true);
        _parts = new List<ResultPlayerView>();
        _Infos.ToList().ForEach(x => {
            _parts.Add(x.GetComponentInChildren<ResultPlayerView>());
        });
    }

    public void SetUp(UnityAction callback)
    {
        _endShowRankingCallback = callback;
    }

    public void FadeIn()
    {
        _fadeImage.gameObject.SetActive(true);
        _fadeImage.color = Color.black;
        var seq = DOTween.Sequence();
        seq.Append(_fadeImage.DOColor(new Color(0, 0, 0, 0), 1.5f).SetEase(Ease.Linear));
        seq.OnComplete(ShowRanking);
    }

    private void ShowRanking()
    {
        var seq = DOTween.Sequence();
        for (int i = 0; i < _Infos.Length; ++i)
        {
            seq.Append(_Infos[i].transform.DOLocalMoveX(0, 1.0f));
        }

        seq.OnComplete(() => {
            _endShowRankingCallback?.Invoke();
        });
    }

    public void SetInfoParts(int index, BattleResultCacheData model)
    {
        _parts[index].SetUp(
            ranking: model.Ranking,
            name: model.Name,
            point: model.Point
            );
    }
}
