using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultPlayerView : MonoBehaviour
{
    [SerializeField] private Text _txtRanking = default;
    [SerializeField] private Text _txtRankingTitle = default;
    [SerializeField] private Text _txtName = default;
    [SerializeField] private Text _txtPoint = default;

    public void SetUp(int ranking, string name, int point)
    {
        _txtRankingTitle.text =
            ranking == 1 ? "st"
            : ranking == 2 ? "nd"
            : ranking == 3 ? "rd"
            : "th";
        _txtRanking.text = ranking.ToString();
        _txtName.text = name;
        _txtPoint.text = point.ToString();
    }
}
