﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;

public class DebugTitleView : MonoBehaviour
{
    [SerializeField] private Button _btnCsvLoad = default;
    [SerializeField] private Button _btnCsvSave = default;

    private string SheetId = "1KzR8kmXS3e1164co5ErUn_dI2M3SuHssMSSHqC64qzg";
    private string SheetName = "Skill";

    private string _saveName = "sample.txt";

    private UnityEvent OnLoadEnd;

    [SerializeField] private Dictionary<int, List<string>> _datas = new Dictionary<int, List<string>>();
    [SerializeField] private List<string> _headerList = new List<string>();

    public void OnClickCsvLoad() => StartCoroutine(LoadCsv());

    public void OnCLickCsvSave() {
        StreamWriter sw;
        FileInfo fi;

        var savePath = Application.dataPath + "/" + _saveName;
        Debug.Log(savePath);
        fi = new FileInfo(savePath);
        sw = fi.AppendText();

        // ヘッダーを出力
        sw.WriteLine(GetHeaderString());

        sw.WriteLine(GetDataString());

        sw.Flush();
        sw.Close();
    }

    private string GetHeaderString() {
        string ret = default;
        for (int i=0; i<_headerList.Count; i++) {
            ret += _headerList[i];
            if (i != _headerList.Count-1) {
                ret += ",";
            }
        }

        ret += "\n";

        return ret;
    }

    private string GetDataString() {
        string ret = default;
        for (int j=0; j<_datas.Count; j++) {
            int no = j + 1;
            int count = _datas[no].Count;
            Debug.Log(count.ToString());
            for (int i=0; i<count; i++) {
                ret += _datas[no][i];
                if (i != _datas[no].Count - 1) {
                    ret += ",";
                }
            }
            ret += "\n";
        }

        return ret;
    }

    IEnumerator LoadCsv() {
        _btnCsvLoad.interactable = false;

        var url = "https://docs.google.com/spreadsheets/d/" + SheetId +
            "/gviz/tq?tqx=out:csv&Sheet=" + SheetName;
        UnityWebRequest request = UnityWebRequest.Get(url);
        yield return request.SendWebRequest();

        _btnCsvLoad.interactable = true;

        var isProtocalError = request.result == UnityWebRequest.Result.ProtocolError;
        var isConnectError = request.result == UnityWebRequest.Result.ConnectionError;
        if (isProtocalError || isConnectError) {
            Debug.LogError(request.error);
        } else {
            ParseCsv(request.downloadHandler.text);
        }
    }

    public void ParseCsv(string text) {
        StringReader reader = new StringReader(text);
        int linecount = 0;
        while (reader.Peek() > -1) {
            string line = reader.ReadLine();
            string[] datas = line.Split(',');
            Debug.Log(datas.ToString());
            if (linecount == 0) {
                _headerList.AddRange(datas); // ヘッダーは別管理
            } else {
                for (int i=0; i<datas.Length; i++) {
                    datas[i] = datas[i].TrimStart('"').TrimEnd('"');
                }
                int datano = int.Parse(datas[0]);
                if (_datas.ContainsKey(datano)) {
                    _datas.Remove(datano); // 既に存在した場合は削除してから追加
                }
                _datas[datano] = new List<string>(datas);
            }
            ++linecount;
        }
    }
}
