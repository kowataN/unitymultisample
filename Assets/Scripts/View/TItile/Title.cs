﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Title : MonoBehaviour
{
    [Header("User Input Panel")]
    [SerializeField] private GameObject _inputPanel;
    [SerializeField] private InputField _userInput;

    [Header("User Guide Panel")]
    [SerializeField] private GameObject _guidePanel;
    [SerializeField] private Text _guideText;

    [SerializeField] private GameObject _btnStart;

    // Use this for initialization
    void Start()
    {
        // TODO: 起動時にセーブデータを読み込み、名前を取得する
        //       名前があったらエディットボックスを表示しない。
        // セーブデータの確認
        bool ExistSaveData = SaveDataModel.Instance.Load();
        if (ExistSaveData)
        {
            // セーブデータがあるのでプレイヤー名を取得してみる
            bool isEmpty = string.IsNullOrEmpty(SaveDataModel.Instance.PlayerModel.PlayerName);
            if (string.IsNullOrEmpty(SaveDataModel.Instance.PlayerModel.PlayerName) == false)
            {
                _guideText.text = "ようこそ、" + SaveDataModel.Instance.PlayerModel.PlayerName + "さん。";
            }
            else
            {

            }
        }

        _inputPanel.SetActive(ExistSaveData == false);
        _guidePanel.SetActive(ExistSaveData == true);

        // セーブデータ確認 デバッグ用
        //if (true) {
        //    PlayerData.Instance.SetPlayerData("hogehoge");
        //    UnitModel unit = new UnitModel();
        //    unit.SetParameter(1,
        //                      TAttribute.kDarkness,
        //                      TTribe.kBasic,
        //                      10000, 1000, 222, 333, 444, 555, "huga"
        //                     );
        //    BasicStatus addStatus;
        //    addStatus.Hp = addStatus.Mp = 221;
        //    addStatus.Atk = addStatus.Mgc =
        //        addStatus.Def = addStatus.Luc = 2000;
        //    PlayerData.Instance.SetUnitData(unit, addStatus);
        //    PlayerData.Instance.Save();
        //    //PlayerData.Instance.Load();
        //}
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void OnClick()
    {
        _btnStart.SetActive(false);

        SaveDataModel.Instance.Save();

        SceneManager.LoadScene("CharaSelect", LoadSceneMode.Single);
    }
    public void OnEditEnd()
    {
        SaveDataModel.Instance.SetPlayerData(_userInput.text);
    }
}
