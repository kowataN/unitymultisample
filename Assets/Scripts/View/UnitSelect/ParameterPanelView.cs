using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class ParameterPanelView : MonoBehaviour
{
    [SerializeField] private Text _name = default;
    [SerializeField] private Text _hp = default;
    [SerializeField] private Text _mp = default;
    [SerializeField] private Text _atk = null;
    [SerializeField] private Text _mgc = default;
    [SerializeField] private Text _def = default;
    [SerializeField] private Text _luc = default;
    [SerializeField] private Text _skill = default;
    [SerializeField] private Button _btnBack = default;
    [SerializeField] private Button _btnSkill = default;

    public IObservable<Unit> OnClickBack => _btnBack.OnClickAsObservable();
    public IObservable<Unit> OnClickSkill => _btnSkill.OnClickAsObservable();

    public void SetActive(bool value)
    {
        _btnBack.interactable = value;
        _btnSkill.interactable = value;
        this.gameObject.SetActive(value);
    }
    
    public void ResetParameter(UnitModel unitModel)
    {
        var unitStatus = unitModel.UnitStatus;
        _name.text = unitStatus.Name;
        _hp.text = unitStatus.Status.HP.ToString();
        _mp.text = unitStatus.Status.MP.ToString();
        _atk.text = unitStatus.Status.Attack.ToString();
        _mgc.text = unitStatus.Status.Magic.ToString();
        _def.text = unitStatus.Status.Defense.ToString();
        _luc.text = unitStatus.Status.Luck.ToString();

        _skill.text = "";
        foreach (var skill in unitModel.SkillList)
        {
            _skill.text += skill.Name + Environment.NewLine;
        }
    }
}
