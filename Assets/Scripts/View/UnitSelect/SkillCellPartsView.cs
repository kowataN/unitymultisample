using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using System;

public class SkillCellPartsView : MonoBehaviour
{
    [SerializeField] private Toggle _select = null;
    [SerializeField] private Text _skillName = null;
    [SerializeField] private Text _cost = null;

    private int _skillNo = 0;
    public int SkillNo => _skillNo;

    public IObservable<bool> OnValueChanged => _select.OnValueChangedAsObservable();

    public void Setup(int skillNo, string skillName, int cost)
    {
        _skillNo = skillNo;
        _skillName.text = skillName;
        _cost.text = "MP" + cost.ToString();
    }

    public bool Selected
    {
        get => _select.isOn;
        set => _select.isOn = value;
    }

    public bool SelectInteractable
    {
        set => _select.interactable = value;
    }
}
