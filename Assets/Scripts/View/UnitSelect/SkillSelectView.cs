using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class SkillSelectView : MonoBehaviour
{
    [SerializeField] private Text _hp = null;
    [SerializeField] private Text _mp = null;
    [SerializeField] private Text _atk = null;
    [SerializeField] private Text _mgc = null;
    [SerializeField] private Text _selectCount = null;
    [SerializeField] private Button _btnBack = null;
    [SerializeField] private Button _btnStart = null;
    [SerializeField] private GameObject _scrollContent = null;
    [SerializeField] private GameObject _skillPrefab = null;

    private Action _onSelected = null;
    private List<SkillCellPartsView> _partsList = new List<SkillCellPartsView>();
    public List<SkillCellPartsView> PartsList => _partsList;

    public IObservable<Unit> OnClickBack => _btnBack.OnClickAsObservable();
    public IObservable<Unit> OnClickStart => _btnStart.OnClickAsObservable();

    public int SelectCount
    {
        get => int.Parse(_selectCount.text.ToString());
        set
        {
            _selectCount.text = value.ToString();
            _selectCount.color = (value == 6) ? Color.red : Color.black;
        }
    }

    public void SetOnSelected(Action act) => _onSelected = act;

    public void SetActive(bool value)
    {
        _btnBack.interactable = value;
        _btnStart.interactable = value;
        this.gameObject.SetActive(value);
    }

    public void SetSkillList(UnitModel unitModel)
    {
        foreach (Transform obj in _scrollContent.transform)
        {
            GameObject.Destroy(obj.gameObject);
        }

        _partsList.Clear();

        foreach (var skill in unitModel.SkillList)
        {
            var obj = Instantiate(_skillPrefab, _scrollContent.transform);
            var skillScr = obj.GetComponentInChildren<SkillCellPartsView>();
            if (skillScr != null)
            {
                skillScr.Setup(
                    skillNo: skill.No,
                    skillName: skill.Name,
                    cost: skill.Cost);
                skillScr.OnValueChanged
                    .Subscribe(_ => {
                        _onSelected?.Invoke();
                    })
                    .AddTo(gameObject);

                _partsList.Add(skillScr);
            }
        }
    }
}
