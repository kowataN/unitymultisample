﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public sealed class UnitSelectView : MonoBehaviour
{
    [SerializeField] private Text _viewTitle = default;
    [SerializeField] private GameObject _characterPosition = default;
    [SerializeField] private Button _btnLeft = default;
    [SerializeField] private Button _btnRight = default;
    [SerializeField] private GameObject[] _unitsPrefabs = default;
    [SerializeField] private ParameterPanelView _parameterPanelView = default;
    [SerializeField] private SkillSelectView _skillSelectView = default;

    public ParameterPanelView ParameterView => _parameterPanelView;
    public SkillSelectView SkillSelectView => _skillSelectView;
    public IObservable<Unit> OnClickLeft => _btnLeft.OnClickAsObservable();
    public IObservable<Unit> OnClickRight => _btnRight.OnClickAsObservable();

    private int _maxCharacter;
   
    public enum ViewMode
    {
        UnitSelect, SkillSelect
    }
    private ViewMode _crtView = ViewMode.UnitSelect;

    private void Awake()
    {
        _parameterPanelView.SetActive(true);
        _skillSelectView.SetActive(false);
    }

    public void Setup(int charaIndex, int maxCharacter)
    {
        _maxCharacter = maxCharacter;
        ChangeCharacter(charaIndex);
    }

    private void UpdateView(int index)
    {
        ResetParameter(index);
        UpdateArrow(index);
    }

    public void ChangeCharacter(int index)
    {
        foreach (Transform t in _characterPosition.transform)
        {
            GameObject.Destroy(t.gameObject);
        }
        // キャラクター変換
        Instantiate(_unitsPrefabs[index], _characterPosition.transform);
        UpdateView(index);
    }

    private void ResetParameter(int index)
    {
        var unitModel = UnitStorage.Inst.GetDataFromIndex(index);
        _parameterPanelView.ResetParameter(unitModel);
    }

    private void UpdateArrow(int index)
    {
        _btnLeft.gameObject.SetActive(index != 0);
        _btnRight.gameObject.SetActive(index != _maxCharacter - 1);
    }

    public void ChangeView(ViewMode view)
    {
        _crtView = view;

        _parameterPanelView.SetActive(_crtView == ViewMode.UnitSelect);
        _skillSelectView.SetActive(_crtView == ViewMode.SkillSelect);

        _viewTitle.text = _crtView == ViewMode.UnitSelect ? "ユニット選択" : "スキル選択";
    }
}
