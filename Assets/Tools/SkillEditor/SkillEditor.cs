﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillEditor : MonoBehaviour {
    [Header("スキル一覧")]
    public GameObject prefabListSkill;
    public ScrollRect listSkill;
    public RectTransform contentListSkill;

    [Header("Basic Panel")]
    public Text textId;
    public Dropdown listType;
    public Dropdown listAttribute;
    public Dropdown listRank;
    public Dropdown ListSpeed;
    public InputField inputName;
    public InputField inputComment;

    [Header("Used Param")]
    public GameObject prefabUsedParam;
    public ScrollRect listUsedParam;
    public RectTransform contentListUsedParam;
    public Button buttonListUsedParamAdd;

    [Header("Target Param")]
    public ScrollRect listTargetParam;
    public RectTransform contentListTargetParam;
    public Button buttonListTargetParamAdd;

    private List<SkillModel> _skillList;
    private int _selectedSkillIndex;

    private void Awake() {
        _skillList = SkillStorage.Inst.SkillList;

        // スキルCSVあれば読み込み
        Debug.Log("SKILL COUNT : " + _skillList.Count);
        for (int i = 0; i < _skillList.Count; ++i) {
            GameObject item = Instantiate(prefabListSkill);
            if (item != null) {
                item.transform.SetParent(contentListSkill, false);
                item.GetComponentInChildren<Text>().text = _skillList[i].No.ToString().PadLeft(4, '0');
                Button button = item.GetComponentInChildren<Button>();
                if (button != null) {
                    button.GetComponentInChildren<Text>().text = _skillList[i].Name;
                    int index = i;
                    button.onClick.AddListener(() => OnSkillListButtonClick(index));
                }
            }
        }

        // 各リスト初期化
        foreach (var val in TypeString.GetStrings()) {
            listType.options.Add(new Dropdown.OptionData { text = val });
        }
        listType.RefreshShownValue(); // 先頭データを選択させる

        foreach (var val in Attribute.GetStrings()) {
            listAttribute.options.Add(new Dropdown.OptionData { text = val });
        }
        listAttribute.RefreshShownValue(); // 先頭データを選択させる

        foreach (var val in Rank.GetSkillRankStrings()) {
            listRank.options.Add(new Dropdown.OptionData { text = val });
        }
        listRank.RefreshShownValue();

        foreach (var val in Speed.GetSkillSpeedStrings()) {
            ListSpeed.options.Add(new Dropdown.OptionData { text = val });
        }
        ListSpeed.RefreshShownValue();

    }
    // Use this for initialization
    void Start() {
        // 戦闘データで初期化する
        SetInfo(_selectedSkillIndex);
    }

    // Update is called once per frame
    void Update() {

    }

    public void OnSkillListButtonClick(int index) {
        _selectedSkillIndex = index;
        SetInfo(_selectedSkillIndex);
    }

    public void SetInfo(int index) {
        ClearContentList(contentListUsedParam);
        ClearContentList(contentListTargetParam);

        textId.text = _skillList[index].No.ToString().PadLeft(6, '0');
        inputName.text = _skillList[index].Name;
        inputComment.text = _skillList[index].Comment;
        listType.value = (int)_skillList[index].Type;
        listAttribute.value = (int)_skillList[index].Atr;
        listRank.value = _skillList[index].Rank;
        ListSpeed.value = _skillList[index].Speed;

        // 消費パラメータ情報設定
        /*
                List<UsedParameter> usedList = _skillList[index].UsedList;
                int usedListCount = _skillList[index].UsedList.Count;
                for (int i = 0; i < usedListCount; ++i)
                {
                    UsedParameter up = usedList[i];
                    up.Index = i;
                    usedList[i] = up;
                }

                for (int i = 0; i < usedList.Count; ++i)
                {
                    GameObject item = Instantiate(prefabUsedParam);
                    if (item != null)
                    {
                        item.transform.SetParent(contentListUsedParam, false);
                    }
                }

                for (int i = 0; i < usedList.Count; ++i)
                {
                    PartsUsedParam item = contentListUsedParam.transform.GetChild(i).GetComponent<PartsUsedParam>();
                    if (item != null)
                    {
                        item.TextParam.text = usedList[i].Parameter + usedList[i].Value.ToString();
                        Button button = item.GetComponentInChildren<Button>();
                        if (button != null)
                        {
                            int btnIndex = i;
                            item.Index = btnIndex;
                            button.onClick.AddListener(() => OnClickUsedListRemove(item));
                        }
                    }
                }
        */

        // オプショナル情報設定
        List < OptionalInfo> optionalList = _skillList[index].OptionalList.Optional;
        for (int i = 0; i < optionalList.Count; ++i) {
            OptionalInfo opt = optionalList[i];
            opt.Index = i;
            optionalList[i] = opt;
        }

        for (int i = 0; i < optionalList.Count; ++i) {
            GameObject item = Instantiate(prefabUsedParam);
            if (item != null) {
                item.transform.SetParent(contentListTargetParam, false);
            }
        }

        for (int i = 0; i < optionalList.Count; ++i) {
            PartsUsedParam item = contentListTargetParam.transform.GetChild(i).GetComponent<PartsUsedParam>();
            if (item != null) {
                item.TextParam.text = optionalList[i].Param.ToString();
                Button button = item.GetComponentInChildren<Button>();
                if (button != null) {
                    int btnIndex = i;
                    item.Index = btnIndex;
                    button.onClick.AddListener(() => OnClickTargetListRemove(item));
                }
            }
        }
    }

    public void OnClickUsedListRemove(PartsUsedParam item) {
        // TODO:
        /*
        for (int i = 0; i < _skillList[_selectedSkillIndex].UsedList.Count; ++i)
        {
            if (_skillList[_selectedSkillIndex].UsedList[i].Index == item.Index)
            {
                _skillList[_selectedSkillIndex].UsedList.RemoveAt(i);
                Destroy(item.gameObject);
                return;
            }
        }*/
    }

    public void OnClickTargetListRemove(PartsUsedParam item) {
        List<OptionalInfo> targetList = _skillList[_selectedSkillIndex].OptionalList.Optional;
        for (int i = 0; i < targetList.Count; ++i) {
            if (targetList[i].Index == item.Index) {
                targetList.RemoveAt(i);
                Destroy(item.gameObject);
                return;
            }
        }
    }

    private void ClearContentList(RectTransform content) {
        foreach (Transform info in content.gameObject.transform) {
            Destroy(info.gameObject);
        }
    }
}
