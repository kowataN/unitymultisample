@startuml

namespace Entity {
    enum Attributes:属性 {
        None:無属性
        Fire:火属性
        Water:水属性
        Thunder:雷属性
        Metal:金属性
        Light:光属性
        Darkness:闇属性
    }

    namespace Skill:スキル {
        enum SkillTypes:スキル種別 {
            None:なし
            Attack:物理攻撃
            Magic:魔法攻撃
            Guard:物理防御
            Shield:魔法防御
            Assist:補助
        }

        class UsedParam:スキル使用時の消費パラメータ <<struct>> <<Serializable>> {
            + パラメータ:string
            + 数値:int
            + インデックス:int
        }

        class SkillTarget:スキル対象情報 <<struct>> <<Serializable>> {
            + 対象:string
            + メンバー:int
            + ターン数:int
            + ポラメータ:string
            + 効果値:int
            + アニメーション:string[]
            + インデックス:int
        }
    }

    namespace Unit:ユニット {
        enum Parameter:パラメーター {
            Hp:ヒットポイント
            Mp:マジックポイント
            Atk:物理攻撃力
            Mgc:魔法攻撃力
            Def:防御力
            Luc:運
            Max:最大値
        }

        enum Tribes:種族の定義 {
            None:なし
            Basic:基本種
            Faint:基本種淡
            Deep:基本種濃
            Enhances:強化種
            Variant:変異種
        }

        class BasicStatus:基礎ステータス <<struct>> {
            + ヒットポイント:int
            + マジックポイント:int
            + 物理攻撃力:int
            + 魔法攻撃力:int
            + 防御力:int
            + 運:int
        }

        class UnitInfo:ユニット情報 <<struct>> {
            + 番号:int
            + 属性:Attributes
            + 種族:Tribes
            + ステータス:BasicStatus
            + スキルセット:int[]
            + 使用フラグ:bool
            + 名前:string
        }
    }

    namespace Item:アイテム {
        class Equip:装備品 {
            + ID:ID
            + Type:種類
            + Rank:ランク
            + EffectInfo:効果情報
        }
    }

    namespace Players:プレイヤー {
        class UnitInfo:ユニット情報 <<struct>> <<Serializable>> {
            + レート : int
            + 名前 : string
            + 寿命 : int
            + ユニットモデル : UnitModel
            + 初期ステータス : BasicStatus
            + 成長ステータス : BasicStatus
            + 装備品インデックス : int
            + 覚えているスキル : int?
            + 覚えられるスキル : int?
            + スキルセット : int(（削除、融合素材、融合元、魂化、等）)
            + 状態 : int
            + 魂化日 : timestamp
            + 作成日 : timestamp
            + 更新日 : timestamp
        }

        class プレイヤー情報 <<struct>> <<Serializable>> {
            + 名前 : string
            + ユニット情報 : UnitInfo
        }
    }
}

@enduml